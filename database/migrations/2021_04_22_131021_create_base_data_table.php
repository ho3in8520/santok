<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_data', function (Blueprint $table) {
            $table->id();
            $table->string('type','100');
            $table->string('value','100');
            $table->integer('parent_id')->default(0);
            $table->boolean('status')->default(1)->comment('0 = deActive , 1 = Active');
            $table->string('extra_value','500')->default(0);
            $table->string('extra_value1','500')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_data');
    }
}
