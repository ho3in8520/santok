<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInformationMajorToAcademicStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('academic_students', function (Blueprint $table) {
            $table->text('information_major')->nullable()->collation('utf8_general_ci');
            $table->string('expert_major')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('academic_students', function (Blueprint $table) {
            $table->removeColumn('information_major');
        });
    }
}
