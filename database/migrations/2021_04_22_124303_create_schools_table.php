<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('code');
            $table->string('name',255);
            $table->string('phone',12);
            $table->unsignedBigInteger('state');
            $table->foreign('state')->references('id')->on('cities');
            $table->unsignedBigInteger('city');
            $table->foreign('city')->references('id')->on('cities');
            $table->string('address',500);
            $table->decimal('location_lat',9,6);
            $table->decimal('location_lon',9,6);
            $table->boolean('status')->default(1)->comment('0=>inactive 1=>active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
