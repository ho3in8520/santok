<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->enum('nationality', ['iran', 'foreign'])->default('iran');
            $table->string('zone');
            $table->string('street');
            $table->string('alley')->nullable();
            $table->string('deadend')->nullable();
            $table->integer('plaque')->default(0);
            $table->integer('postal_code')->default(0);
            $table->enum('ownership', ['personal', 'tenant'])->default('personal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn('nationality');
            $table->dropColumn('zone');
            $table->dropColumn('street');
            $table->dropColumn('alley');
            $table->dropColumn('deadend');
            $table->dropColumn('plaque');
            $table->dropColumn('postal_code');
            $table->dropColumn('ownership');
        });
    }
}
