<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PanelType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array=[
            ['id'=>1,'name'=>'ادمین','table_name'=>'admin'],
            ['id'=>2,'name'=>'کارشناس','table_name'=>'expert'],
            ['id'=>3,'name'=>'مدیرمدرسه','table_name'=>'headmaster'],
        ];
        \App\Models\PanelType::insert($array);
    }
}
