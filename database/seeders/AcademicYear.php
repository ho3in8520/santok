<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AcademicYear extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array=[
            ['year'=>'1399-1400'],
            ['year'=>'1401-1402'],
        ];

        collect($array)->each(function ($item) {
            \App\Models\AcademicYear::firstOrCreate($item);
        });
    }
}
