<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ChangeCityStatus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = \App\Models\City::all();
        collect($cities)->each(function ($item) {
            if (!in_array($item->id, [192, 193, 194])) {
                $item->status = 0;
                $item->save();
            }
        });
    }
}
