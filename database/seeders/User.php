<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'first_name'=>'حسین',
            'last_name'=>'شاهپوری',
            'national_code'=>'5490108886',
            'address'=>'تیران',
            'phone'=>'03142222222',
            'mobile'=>'09388159606',
            'panel_type'=>'1',
            'password'=>Hash::make(12345678),
        ]);
    }
}
