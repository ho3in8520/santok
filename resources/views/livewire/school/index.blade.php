<div>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label>کد مدرسه</label>
                <input class="form-control" type="text" wire:model.defer="code">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>نام مدرسه</label>
                <input class="form-control" type="text" wire:model.defer="name">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>تلفن مدرسه</label>
                <input class="form-control" type="text" wire:model.defer="phone">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>استان مدرسه</label>
                <select class="form-control state" wire:model.defer="state">
                    <option value="">انتخاب کنید...</option>
                    @foreach($states as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>شهر مدرسه</label>
                <select class="form-control city" wire:model.defer="city">
                    <option value="">انتخاب کنید...</option>
                    @foreach($cities as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group mt-4">
                <button type="button" class="btn btn-primary" wire:click="query"><i class="fa fa-search"></i> اعمال
                    فیلتر
                </button>
            </div>
        </div>
    </div>
    <div class="summary">{!!getSummary($schools) !!}</div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th scope="col">کد مدرسه</th>
                <th scope="col">نام مدرسه</th>
                <th scope="col">تلفن</th>
                <th scope="col">استان/شهر</th>
                <th scope="col">وضعیت</th>
                <th scope="col">عملیات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($schools as $item)
                <tr>
                    <td>{{ $item->code }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->phone }}</td>
                    <td>{{ $item->stateModel->name.'/'.$item->cityModel->name }}</td>
                    <td>
                        @if($item->status==1)
                            <span class="badge badge-success">فعال</span>
                        @else
                            <span class="badge badge-danger">غیرفعال</span>
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-info btn-sm text-white" href="{{ route('school.edit',$item->id) }}" title="ویرایش"><i
                                class="fa fa-pencil-alt"></i> </a>
                        @if($item->status==1)
                            <a title="غیرفعال" class="btn btn-danger btn-sm text-white pointer"
                               wire:click="swal({{ $item->id }})"><i class="fa fa-times"></i> </a>
                        @else
                            <a title="فعال" class="btn btn-success btn-sm text-white pointer"
                               wire:click="swal({{ $item->id }})"><i class="fa fa-check"></i> </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $schools->links('livewire.component.custom-pagination-links-view') !!}
    </div>
</div>
