<div>
    <form wire:submit.prevent="submit">
        <div class="row">
            <div class="col-md-2">
                <div class="form-group @error('code') has-danger @enderror">
                    <label>کد مدرسه</label>
                    <input type="text" class="form-control" wire:model.defer="code">
                    @error('code') <span class="text text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group @error('name') has-danger @enderror">
                    <label>نام مدرسه</label>
                    <input type="text" class="form-control" wire:model.defer="name">
                    @error('name') <span class="text text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group @error('phone') has-danger @enderror">
                    <label>تلفن مدرسه</label>
                    <input type="text" class="form-control" wire:model.defer="phone">
                    @error('phone') <span class="text text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group @error('state') has-danger @enderror">
                    <label>استان مدرسه</label>
                    <select class="form-control" wire:model="state" wire:change="cities">
                        <option value="">انتخاب کنید...</option>
                        @foreach($states as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                    @error('state') <span class="text text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group @error('city') has-danger @enderror">
                    <label>شهر مدرسه</label>
                    <select class="form-control" wire:model.defer="city">
                    <option value="">انتخاب کنید...</option>
                    @foreach($cities as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                    </select>
                    @error('city') <span class="text text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group @error('major') has-danger @enderror" wire:ignore>
                    <label>رشته تحصیلی</label>
                    <select class="form-control select2-multiple" multiple="multiple">
                    @foreach($majors as $item)
                        <option value="{{ $item }}" selected="selected">{{ $item }}</option>
                    @endforeach
                    </select>
                    @error('major') <span class="text text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group @error('address') has-danger @enderror">
                    <label>آدرس</label>
                    <textarea rows="3" class="form-control" wire:model.defer="address"></textarea>
                    @error('address') <span class="text text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>موقیعت مکانی رو نقشه</label>
                    @component('component.leafletMap')
                        @if(!empty($school))
                            @slot('lat',$school->location_lat)
                            @slot('lng',$school->location_lon)
                            @endif
                    @endcomponent
                    @error('lat') <span class="text text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-success">ثبت</button>
                </div>
            </div>
        </div>
    </form>
</div>
