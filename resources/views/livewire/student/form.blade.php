<div>
    <div>
        <form wire:submit.prevent="{{ !empty($student) && isset($student['id']) ? 'update' : 'submit' }}">
            <div class='row'>
                <div class="col-md-2">
                    <div class="form-group">
                        @if (!empty($imageStudent))
                            <i id='deleteImageUpload' title="حذف عکس" class="fa fa-trash text-danger"
                                wire:click="removePreview"></i>
                        @else
                            <div id="deleteImage">
                            </div>
                        @endif
                        <input accept="image/jpeg" wire:model.defer="imageStudent" wire:change="changePreview"
                            type="file" id="imgInp" style="display: none">
                        <img title="آپلود عکس" style="cursor:pointer;" id="blah"
                            src="{{ !empty($preview) ? $preview : asset('/default-image/avatar.jpg') }}"
                            alt="your image" width="150px" height="150px">
                    </div>
                    @error('student.imageStudent')
                        <span class="text text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group @error('student.first_name') has-danger @enderror">
                        <label>نام</label>
                        <span class="text-danger">*</span>
                        <input type="text" class="form-control" wire:model.defer="student.first_name">
                        @error('student.first_name')
                            <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group @error('student.last_name') has-danger @enderror">
                        <label>نام خانوادگی</label>
                        <span class="text-danger">*</span>
                        <input type="text" class="form-control" wire:model.defer="student.last_name">
                        @error('student.last_name')
                            <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group @error('student.father_name') has-danger @enderror">
                        <label>نام پدر</label>
                        <span class="text-danger">*</span>
                        <input type="text" class="form-control" wire:model.defer="student.father_name">
                        @error('student.father_name')
                            <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group @error('student.date_birth') has-danger @enderror">
                        <label>تاریخ تولد</label>
{{--                        <span class="text-danger">*</span>--}}
                        <input type="text" class="form-control datePicker" wire:model.defer="student.date_birth">
                        @error('student.date_birth')
                            <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group @error('student.national_code') has-danger @enderror">
                        <label>کدملی</label>
                        <span class="text-danger">*</span>
                        <input type="text" maxlength="10" class="form-control"
                            wire:model.defer="student.national_code">
                        @error('student.national_code')
                            <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                {{-- <div class="col-md-2">
                    <div class="form-group @error('student.phone') has-danger @enderror">
                        <label>تلفن ثابت</label>
                        <span class="text-danger">*</span>
                        <input type="text" maxlength="11" class="form-control" wire:model.defer="phone">
                        @error('student.phone') <span class="text text-danger">{{ $message }}</span> @enderror
                    </div>
                </div> --}}
                <div class="col-md-2">
                    <div class="form-group @error('student.mobile') has-danger @enderror">
                        <label>تلفن همراه</label>
                        <span class="text-danger">*</span>
                        <input type="text" maxlength="11" class="form-control" wire:model.defer="student.mobile">
                        @error('student.mobile')
                            <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group @error('student.nationality') has-danger @enderror">
                        <label>تابعیت</label>
                        <span class="text-danger">*</span>
                        <select class="form-control" wire:model.defer="student.nationality">
                            <option value="">انتخاب کنید...</option>
                            <option value="iran" selected>ایرانی</option>
                            <option value="foreign">خارجی</option>
                        </select>
                        @error('student.nationality')
                            <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                {{-- <div class="col-md-2">
                    <div class="form-group @error('student.state') has-danger @enderror">
                        <label>استان</label>
                        <span class="text-danger">*</span>
                        <select class="form-control state" wire:model.defer="state">
                            <option value="">انتخاب کنید...</option>
                            @foreach ($states as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('student.state') <span class="text text-danger">{{ $message }}</span> @enderror
                    </div>
                </div> --}}
                <div class="col-md-2">
                    <div class="form-group @error('student.city') has-danger @enderror">
                        <label>شهر</label>
                        <span class="text-danger">*</span>
                        <select class="form-control city" wire:model.defer="student.city"
                            wire:change="zone($event.target.value)">
                            <option value="">انتخاب کنید...</option>
                            @foreach ($cities as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('student.city')
                            <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                {{-- @dd($modeZone) --}}
                @if (in_array($city_id, [192, 193, 194]))
                    <div class="col-md-2">
                        <div class="form-group @error('student.zone') has-danger @enderror">
                            <label>نام منطقه/روستا</label>
                            <span class="text-danger">*</span>
                            <select class="form-control" wire:model.defer="student.zone">
                                <option value="">انتخاب کنید...</option>
                                @foreach ($arrayZones[$city_id] as $key => $item)
                                    <option value="{{ $item }}">{{ $item }}</option>
                                @endforeach
                            </select>
                            @error('student.zone')
                                <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                @else
                    <div class="col-md-2">
                        <div class="form-group @error('student.zone') has-danger @enderror">
                            <label>نام شهر/روستا</label>
                            <span class="text-danger">*</span>
                            <input type="text" class="form-control" wire:model.defer="student.zone">
                            @error('student.zone')
                                <span class="text text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                @endif
            {{--    <div class="col-md-2">
                    <div class="form-group @error('student.street') has-danger @enderror">
                        <label>خیابان</label>
--}}{{--                        <span class="text-danger">*</span>--}}{{--
                        <input type="text" class="form-control" wire:model.defer="student.street">
                        @error('student.street')
                            <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group @error('student.alley') has-danger @enderror">
                        <label>کوچه</label>
                        <input type="text" class="form-control" wire:model.defer="student.alley">
                        @error('student.alley')
                            <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>--}}
                {{-- <div class="col-md-2">
                    <div class="form-group @error('student.deadend') has-danger @enderror">
                        <label>بن بست</label>
                        <input type="text" class="form-control" wire:model.defer="deadend">
                        @error('student.deadend') <span class="text text-danger">{{ $message }}</span> @enderror
                    </div>
                </div> --}}
                {{-- <div class="col-md-2">
                    <div class="form-group @error('student.plaque') has-danger @enderror">
                        <label>پلاک</label>
                        <span class="text-danger">*</span>
                        <input type="text" maxlength="3" class="form-control" wire:model.defer="plaque">
                        @error('student.plaque') <span class="text text-danger">{{ $message }}</span> @enderror
                    </div>
                </div> --}}
                {{-- <div class="col-md-2">
                    <div class="form-group @error('student.postal_code') has-danger @enderror">
                        <label>کد پستی</label>
                        <span class="text-danger">*</span>
                        <input type="text" maxlength="10" class="form-control" wire:model.defer="postal_code">
                        @error('student.postal_code') <span class="text text-danger">{{ $message }}</span> @enderror
                    </div>
                </div> --}}
                {{-- <div class="col-md-2">
                    <div class="form-group @error('student.ownership') has-danger @enderror">
                        <label>مالکیت</label>
                        <span class="text-danger">*</span>
                        <select class="form-control" wire:model.defer="ownership">
                            <option value="">انتخاب کنید...</option>
                            <option value="personal">شخصی</option>
                            <option value="tenant">استیجاری</option>
                        </select>
                        @error('student.ownership') <span class="text text-danger">{{ $message }}</span> @enderror
                    </div>
                </div> --}}
                {{-- <div class="col-md-12"> --}}
                {{-- <div class="form-group @error('student.address') has-danger @enderror"> --}}
                {{-- <label>آدرس</label> --}}
                {{-- <textarea rows="3" class="form-control" wire:model.defer="address"></textarea> --}}
                {{-- @error('student.address') <span class="text text-danger">{{ $message }}</span> @enderror --}}
                {{-- </div> --}}
                {{-- </div> --}}
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success">ثبت</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>
