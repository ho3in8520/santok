<div>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label>نام و نام خانوادگی</label>
                <input class="form-control" type="text" wire:model.defer="name">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>نام پدر</label>
                <input class="form-control" type="text" wire:model.defer="father_name">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>کدملی</label>
                <input class="form-control" type="text" wire:model.defer="national_code">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>استان مدرسه</label>
                <select class="form-control state" wire:model.defer="state">
                    <option value="">انتخاب کنید...</option>
                    @foreach($states as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>شهر مدرسه</label>
                <select class="form-control city" wire:model.defer="city">
                    <option value="">انتخاب کنید...</option>
                    @foreach($cities as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group mt-4">
                <button type="button" class="btn btn-primary" wire:click="query"><i class="fa fa-search"></i> اعمال
                    فیلتر
                </button>
            </div>
        </div>
    </div>
    <div class="summary">{!!getSummary($students) !!}</div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">نام</th>
                <th scope="col">نام خانوادگی</th>
                <th scope="col">نام پدر</th>
                <th scope="col">کدملی</th>
                <th scope="col">استان/شهر</th>
                <th scope="col">عملیات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($students as $item)
                <tr>
                    <td>{{ index($students,$loop) }}</td>
                    <td>{{ $item->first_name }}</td>
                    <td>{{ $item->last_name }}</td>
                    <td>{{ $item->father_name }}</td>
                    <td>{{ $item->national_code  }}</td>
                    <td>{{ $item->fullAddress }}</td>
                    <td>
                        @can('student-edit')
                            <a class="btn btn-info btn-sm text-white" href="{{ route('student.edit',$item->id) }}"
                               title="ویرایش"><i
                                    class="fa fa-pencil-alt"></i> </a>
                        @endcan
                        @can('student-delete')
                            <a class="btn btn-danger btn-sm text-white pointer"
                               wire:click="questionDelete({{ $item->id }})"
                               title="حذف"><i
                                    class="fa fa-trash"></i> </a>
                        @endcan
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $students->links('livewire.component.custom-pagination-links-view') !!}
    </div>
</div>
