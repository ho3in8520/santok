<div class="w-100">
    <div class="row">
        <div class="col-6">
            <div class="card m-b-30">
                <div class="card-body">
                    <form wire:submit.prevent="submit">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group @error('year') has-danger @enderror">
                                    <label>سال تحصیلی</label>
                                    <input type="text" class="form-control" wire:model.defer="year"
                                           placeholder="1400-1401">
                                    @error('year') <span class="text text-danger">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">ثبت</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card m-b-30">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">سال تحصیلی</th>
                                <th scope="col">عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($yearsItem->isNotEmpty())
                                @foreach($yearsItem as $row)
                                    <tr>
                                        <th scope="row">{{ index($yearsItem,$loop) }}</th>
                                        <td>{{ $row->year }}</td>
                                        <td>
                                            <a title="ویرایش" class="btn btn-info btn-sm" href="{{ route('academic.years.edit',$row->id) }}"><i
                                                    class="fa fa-pencil-alt"></i> </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <th colspan="3" class="text-center">موردی یافت نشد!</th>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        {!! $yearsItem->links('livewire.component.custom-pagination-links-view') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
