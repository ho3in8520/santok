<div>
    <div class="card-body">
        <div class="p-2">
            <h4 class="text-muted float-right font-18 mt-4">ورود</h4>
            <div>
                <a href="index-2.html" class="logo logo-admin"><img
                            src="{{ asset('theme/assets/images/logo_dark.png') }}"
                            height="28" alt="logo"></a>
            </div>
        </div>

        <div class="p-2">
            <form class="form-horizontal m-t-20" wire:submit.prevent="login">

                <div class="form-group row">
                    <div class="col-12 @error('national_code') has-danger @enderror">
                        <input class="form-control" wire:model.defer="national_code" type="text"
                               placeholder="کدملی" maxlength="10">
                        @error('national_code')
                        <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12 @error('password') has-danger @enderror">
                        <input class="form-control" type="password" wire:model.defer="password" placeholder="رمز عبور">
                        @error('password')
                        <span class="text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row" wire:ignore>
                    <div class="col-12 @error('password') has-danger @enderror">
                        {!! htmlFormSnippet([
      "theme" => "light",
      "size" => "normal",
      "tabindex" => "3",
      "callback" => "callbackFunction",
  ]) !!}
                    </div>
                </div>
                @error('recaptcha')
                <span class="text text-danger">{{ $message }}</span>
                @enderror

                <div class="form-group row">
                    <div class="col-12">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label" for="customCheck1">مرا به خاطر داشته
                                باش</label>
                        </div>
                    </div>
                </div>

                <div class="form-group text-center row m-t-20">
                    <div class="col-12">
                        <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">
                            ورود
                        </button>
                    </div>
                </div>

                {{--                            <div class="form-group m-t-10 mb-0 row">--}}
                {{--                                <div class="col-sm-7 m-t-20">--}}
                {{--                                    <a href="pages-recoverpw.html" class="text-muted"><i class="mdi mdi-lock"></i>--}}
                {{--                                        فراموشی رمز عبور؟</a>--}}
                {{--                                </div>--}}
                {{--                                <div class="col-sm-5 m-t-20">--}}
                {{--                                    <a href="pages-register.html" class="text-muted"><i--}}
                {{--                                            class="mdi mdi-account-circle"></i> ایجاد حساب جدید</a>--}}
                {{--                                </div>--}}
                {{--                            </div>--}}
            </form>
        </div>

    </div>
</div>
@section('script')
    <script>
        function callbackFunction() {
            var result = $(".g-recaptcha-response").val();
            Livewire.emit('recaptcha', result)
        }
    </script>
@append