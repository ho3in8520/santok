<div class="row">
    <input type="hidden" value="{{ $result->id }}" name="id">
    <div class="col-md-4">
        <label>پایه تحصیلی</label>
        <select class="form-control levels-edit" name="levels">
            <option value="">انتخاب کنید...</option>
            @foreach($levels as $key=>$row)
                <option value="{{ $key }}" {{ (!empty($result->levels) && $result->levels==$key)?'selected':'' }}>{{ $row }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        <label>مدرسه</label>
        <select class="form-control" name="school">
            <option value="">انتخاب کنید...</option>
            @foreach($schools as $row)
                <option value="{{ $row->id }}" {{ (!empty($result->expert_school_id) && $result->expert_school_id==$row->id)?'selected':'' }}>{{ $row->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4 major-edit" style="display: {{ ($result->levels >= 10)?'block':'none' }}">
        <label>رشته تحصیلی</label>
        <select class="form-control" name="major">
            @if(empty($major_option))
                <option value="">انتخاب کنید...</option>
            @endif
            {!! $major_option !!}
        </select>
    </div>
</div>
