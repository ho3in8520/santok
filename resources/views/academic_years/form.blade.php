@extends('master_page')
@section('title_browser')
    سال تحصیلی
@endsection
@section('style')
    <link href="{{ asset('general/map/leaflet.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">سال تحصیلی</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        @if(isset($result))
        <livewire:academic-years.form :yearEdit="$result"/>
        @else
            <livewire:academic-years.form />
        @endif
    </div>
@endsection
@section('script')
    <script src="{{ asset('general/map/leaflet.js') }}"></script>
@endsection
