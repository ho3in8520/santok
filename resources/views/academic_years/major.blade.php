<div class="col-md-12 major-form">
{{--    <hr>
    <div class="row">
        <div class="col-md-2">
            <p class="mt-2">اولویت اول</p>
        </div>
        @foreach(json_decode($result->information_major)->first_priority as $item)
        <div class="col-md-2">
          <input class="form-control" value="{{ $item }}" readonly>
        </div>
          @endforeach
    </div>
    <div class="row">
        <div class="col-md-2">
            <p class="mt-2">اولویت دوم</p>
        </div>
        @foreach(json_decode($result->information_major)->second_priority as $item)
            <div class="col-md-2">
             <input class="form-control" value="{{ $item }}" readonly>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-2">
            <p class="mt-2">اولویت سوم</p>
        </div>
        @foreach(json_decode($result->information_major)->third_priority as $item)
            <div class="col-md-2">
                <input class="form-control" value="{{ $item }}" readonly>
            </div>
        @endforeach
    </div>
    <hr>--}}
    <div class="row">
        <div class="col-md-2">
            <p class="mt-2">علاقه دانش آموز به رشته</p>
        </div>
        @foreach(json_decode($result->information_major)->interest as $item)
            <div class="col-md-2">
                <input class="form-control" value="{{ $item }}" readonly>
            </div>
       @endforeach
    </div>
    <hr>
    <div class="row">
        <div class="col-md-3">
            <p class="mt-2">تمایل به تحصیل در مدارس غیرانتفاعی دارد؟</p>
        </div>
        <div class="col-md-2">
            <input class="form-control" value="{{ (json_decode($result->information_major)->private_schools==1)?'بله':'خیر' }}" readonly>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-2">
            <p class="mt-2">معدل پایه نهم</p>
        </div>
        <div class="col-md-2">
            <input class="form-control" value="{{ json_decode($result->information_major)->corrected }}" readonly>
        </div>
    </div>
    <hr>
    <div class="row">
        <input type="hidden" value="{{ $result->id }}" name="id">
        <div class="col-md-2">
            <p class="mt-2">تکمیل اطلاعات</p>
        </div>
        <div class="col-md-2">
            <label>مدرسه</label>
            <select class="form-control" name="school">
                <option value="">انتخاب کنید...</option>
                @foreach($schools as $row)
                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-2">
            <label>رشته تحصیلی</label>
            <select class="form-control" name="major">
                <option value="">انتخاب کنید...</option>
            </select>
        </div>
    </div>
</div>
