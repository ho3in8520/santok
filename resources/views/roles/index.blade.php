@extends('master_page')
@section('title_browser')
    لیست گروه ها
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">لیست گروه ها</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام گروه</th>
                                <th scope="col">تاریخ ایجاد</th>
                                <th scope="col">عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($roles->total() >0)
                            @foreach($roles as $item)
                                <tr>
                                    <td>{{ index($roles,$loop) }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ jdate_from_gregorian($item->created_at) }}</td>
                                    <td>
                                        <a href="{{ route('roles.edit',$item->id) }}" class="btn btn-info btn-sm"
                                           title="ویرایش"><i class="fa fa-pencil-alt"></i> </a>
                                        <a class="btn btn-danger btn-sm text-white delete pointer" data-key="{{ $item->id }}"
                                           title="ویرایش"><i class="fa fa-times"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                            @else
                            <tr>
                                <th colspan="4" class="text-center">موردی یافت نشد</th>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                        {!! $roles->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.delete', function () {
                var id = $(this).data('key');
                swal({
                    title: "اطمینان از انجام عملیات",
                    icon: "warning",
                    buttons: ['انصراف', 'تایید'],
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                type: "DELETE",
                                url: `{{ route('roles.destroy') }}`,
                                data: {_token: `{{ csrf_token() }}`, id: id},
                                success: function (response) {
                                    if (response.status == 100) {
                                        swal(response.msg, '', 'success')
                                        setTimeout(function () {
                                            window.location.reload();
                                        }, 3000)
                                    } else {
                                        swal(response.msg, '', 'error')
                                    }
                                }
                            });
                        }
                    });
            })
        })
    </script>
@endsection
