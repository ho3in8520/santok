@extends('master_page')
@section('title_browser')
    لیست درخواست ها
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0"> لیست درخواست ها</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <div class="summary">{!!getSummary($result) !!}</div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام و نام خانوادگی</th>
                                <th scope="col">نام پدر</th>
                                <th scope="col">تاریخ تولد</th>
                                <th scope="col">کدملی</th>
                                <th scope="col">پایه</th>
                                <th scope="col">نام مدرسه</th>
                                <th scope="col">آدرس</th>
                                <th scope="col">وضعیت</th>
                                <th scope="col">مدرسه مشخص شده توسط کارشناس</th>
                                <th scope="col">رشته مشخص شده توسط کارشناس</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($result->total() >0)
                                @foreach($result as $item)
                                    <tr>
                                        <td>{{ index($result,$loop) }}</td>
                                        <td>{{ $item->student->first_name.' '.$item->student->last_name }}</td>
                                        <td>{{ $item->student->father_name }}</td>
                                        <td>{{ $item->student->date_birth }}</td>
                                        <td>{{ $item->student->national_code  }}</td>
                                        <td>{{ getLevels($item->levels) }}</td>
                                        <td>{{ $item->school->name  }}</td>
                                        <td>{{ $item->student->fullAddress  }}</td>
                                        <td>
                                            @if($item->status==0)
                                                <span class="badge badge-warning">درحال بررسی</span>
                                            @elseif($item->status==1)
                                                <span class="badge badge-success">پاسخ داده شده</span>
                                            @else
                                                <span class="badge badge-danger">رد شده</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ !empty($item->expertSchool)?$item->expertSchool->name:'' }}
                                        </td>
                                        <td>
                                            {{ ($item->expert_major)?$item->expert_major:'ثبت نشده' }}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <th colspan="9" class="text-center">موردی یافت نشد</th>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        {!! $result->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
