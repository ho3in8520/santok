@extends('master_page')
@section('title_browser')
    ثبت دانش آموز در سال تحصیلی جدید
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0"> ثبت دانش آموز در سال تحصیلی جدید</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <form
                        action="{{ (empty($result))?route('student.school.store'):route('student.school.update',$result->id) }}"
                        method="post">
                        @csrf
                        <input type="hidden" name="student" value="{{ $id }}">
                        <input type="hidden" name="school" value="{{ auth()->user()->school }}">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>سال تحصیلی</label>
                                    <select class="form-control" name="academic_years">
                                        <option value="">انتخاب کنید...</option>
                                        @foreach($academic_years as $row)
                                            <option
                                                value="{{ $row->id }}">{{ $row->year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>پایه</label>
                                    <select class="form-control panel-type" name="levels">
                                        <option value="" {{ (request()->query('levels')=='')?'selected':'' }}>انتخاب
                                            کنید...
                                        </option>
                                        <option value="1" {{ (request()->query('levels')==1)?'selected':'' }}>پیش
                                            دبستانی
                                        </option>
                                        <option value="2" {{ (request()->query('levels')==2)?'selected':'' }}>اول
                                        </option>
                                        <option value="3" {{ (request()->query('levels')==3)?'selected':'' }}>دوم
                                        </option>
                                        <option value="4" {{ (request()->query('levels')==4)?'selected':'' }}>سوم
                                        </option>
                                        <option value="5" {{ (request()->query('levels')==5)?'selected':'' }}>چهارم
                                        </option>
                                        <option value="6" {{ (request()->query('levels')==6)?'selected':'' }}>پنجم
                                        </option>
                                        <option value="7" {{ (request()->query('levels')==7)?'selected':'' }}>ششم
                                        </option>
                                        <option value="8" {{ (request()->query('levels')==8)?'selected':'' }}>هفتم
                                        </option>
                                        <option value="10" {{ (request()->query('levels')==10)?'selected':'' }}>نهم
                                        </option>
                                        <option value="11" {{ (request()->query('levels')==11)?'selected':'' }}>دهم
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="major">

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="button" class="btn btn-success ajaxStore">ثبت</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 major-form d-none">
       {{-- <hr>
        <div class="row">
            <div class="col-md-2">
                <p class="mt-2">اولویت اول</p>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="first_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="first_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="first_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="first_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <p class="mt-2">اولویت دوم</p>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="second_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="second_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="second_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="second_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <p class="mt-2">اولویت سوم</p>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="third_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="third_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="third_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="third_priority[]">
                    {!!  academicOrientation() !!}
                </select>
            </div>
        </div>--}}
        <hr>
        <div class="row">
            <div class="col-md-2">
                <p class="mt-2">علاقه دانش آموز به رشته</p>
            </div>

            <div class="col-md-2">
                <select class="form-control" name="interest[]">
                    {!!  $majors !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="interest[]">
                    {!!  $majors !!}
                </select>
            </div>
            <div class="col-md-2">
                <select class="form-control" name="interest[]">
                    {!!  $majors !!}
                </select>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <p class="mt-2">تمایل به تحصیل در مدارس غیرانتفاعی دارد؟</p>
            </div>
            <div class="col-md-5">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="private_schools" id="inlineRadio1" value="1">
                    <label class="form-check-label" for="inlineRadio1"> بله </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="private_schools" id="inlineRadio2" value="0"
                           checked>
                    <label class="form-check-label" for="inlineRadio2"> خیر </label>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-2">
                <p class="mt-2">معدل پایه نهم</p>
            </div>
            <div class="col-md-2">
                <input class="form-control" type="text" name="corrected">
            </div>
        </div>
    </div>
@endsection
 @section('script')
    <script>
        $(document).ready(function () {
            $(document).on('change', '.panel-type', function () {
                var val = $(this).val();
                if (val == 11) {
                    var form = $(".major-form").clone();
                    $(form).removeClass("d-none");
                    $(".major").html(form);
                } else {
                    $(".major").html('');
                }
            })
        })
    </script>
@endsection
