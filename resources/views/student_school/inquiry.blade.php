@extends('master_page')
@section('title_browser')
    استعلام دانش آموز
@endsection
@section('content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h4 class="page-title m-0">استعلام دانش آموز</h4>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end page-title-box -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <form action="" method="get">
                            @csrf
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>کدملی دانش آموز</label>
                                        <input type="text" class="form-control national_code" name="national_code"
                                               value="{{ request()->query('national_code') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success search">استعلام</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <div class="table-responsive">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('general/function/popup.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.search', function () {
                var elem = $(this);
                $(".table-responsive").html("")
                $(".help-block").remove();
                $(elem).closest('form').find('.national_code').closest(".form-group").removeClass("has-danger")
                var val = $(".national_code").val();
                $.get(`{{ route('student.school.inquiry.response') }}`, {
                    _token: `{{ csrf_token() }}`,
                    national_code: val
                }, function (response) {
                    if (response.status == 300) {
                    swal(response.msg,'','error')
                    } else {
                        $(".table-responsive").html(response)
                    }
                }).fail(function (response) {
                    $(elem).closest('form').find('.national_code').closest(".form-group").addClass("has-danger")
                    $(elem).closest('form').find('.national_code').parent().append('<div class="help-block text-danger">' + response.responseJSON.errors.national_code[0] + '</div>')
                })
            })
        })
    </script>
@endsection
