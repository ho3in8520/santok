<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th scope="col">نام و نام خانوادگی</th>
        <th scope="col">نام پدر</th>
        <th scope="col">تاریخ تولد</th>
        <th scope="col">کدملی</th>
        <th scope="col">پایه</th>
        <th scope="col">سال تحصیلی</th>
        <th scope="col">نام مدرسه ثبت کننده</th>
        <th scope="col">آدرس</th>
        <th scope="col">مدرسه مشخص شده توسط کارشناس</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{ $result->first_name.' '.$result->last_name }}</td>
        <td>{{ $result->father_name }}</td>
        <td>{{ $result->date_birth }}</td>
        <td>{{ $result->national_code  }}</td>
        <td>{{ getLevels($result->academicStudent[0]->levels)  }}</td>
        <td>{{ $result->academicStudent[0]->academicYear->year  }}</td>
        <td>{{ $result->academicStudent[0]->school->name  }}</td>
        <td>{{ $result->stateModel->name.' '.$result->cityModel->name.' '.$result->address  }}</td>
        <td>
            {{ (!empty($result->academicStudent[0]->expertSchool))?$result->academicStudent[0]->expertSchool->name:'مشخص نشده' }}
        </td>
        <td>{{ ($result->academicStudent[0]->expert_major)?$result->academicStudent[0]->expert_major:'مشخص نشده'  }}</td>
    </tr>
    </tbody>
</table>
