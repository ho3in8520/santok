@extends('master_page')
@section('title_browser')
    مدرسه جدید
@endsection
@section('style')
    <link href="{{ asset('general/map/leaflet.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">ثبت مدارس</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    @if(isset($school))
                        <livewire:school.form :states="$states" :school="$school" :cities="$cities"/>
                    @else
                        <livewire:school.form :states="$states"/>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('general/map/leaflet.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".select2-multiple").select2({
                tags: true
            });

            $(".select2-multiple").change(function () {
                window.livewire.emit('major',$(".select2-multiple").val())
            })
        })
    </script>
@endsection
