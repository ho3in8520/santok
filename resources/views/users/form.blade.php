@extends('master_page')
@section('title_browser')
    کاربر جدید
@endsection
@section('style')
    <style>

        #deleteImage {
            position: relative;
        }

        #deleteImageUpload {
            font-size: 35px;
            margin-left: 10px;
            z-index: 1;
            cursor: pointer;
            position: absolute;
        }

        .preview {
            overflow: hidden;
            width: 160px;
            height: 160px;
            margin: 10px;
            border: 1px solid red;
            direction: ltr !important;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">کاربر جدید</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <form action="{{ (empty($result))?route('users.store'):route('users.update',$result->id) }}"
                          method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>نام</label>
                                    <input type="text" class="form-control" name="first_name"
                                           value="{{ (!empty($result))?$result->first_name:'' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>نام خانوادگی</label>
                                    <input type="text" class="form-control" name="last_name"
                                           value="{{ (!empty($result))?$result->last_name:'' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>کدملی</label>
                                    <input type="text" maxlength="10" class="form-control" name="national_code"
                                           value="{{ (!empty($result))?$result->national_code:'' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>تلفن ثابت</label>
                                    <input type="text" maxlength="11" class="form-control" name="phone"
                                           value="{{ (!empty($result))?$result->phone:'' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>تلفن همراه</label>
                                    <input type="text" maxlength="11" class="form-control" name="mobile"
                                           value="{{ (!empty($result))?$result->mobile:'' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>پکیج دسترسی</label>
                                    <select class="form-control" name="role">
                                        <option value="">انتخاب کنید...</option>
                                        @foreach($roles as $row)
                                            <option
                                                value="{{ $row->id }}" {{ (!empty($result->roles[0]) && $result->roles[0]->id==$row->id)?'selected':'' }}>{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>نوع پنل</label>
                                    <select class="form-control panel-type" name="panel_type">
                                        <option value="">انتخاب کنید...</option>
                                        @foreach($panel_types as $row)
                                            <option
                                                value="{{ $row->id }}" {{ (!empty($result) && $result->panel_type==$row->id)?'selected':'' }}>{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>انتخاب مدرسه برای ثبت دانش آموز</label>
                                    <select class="form-control school" name="school">
                                        <option value="">انتخاب کنید...</option>
                                        @foreach($schools as $row)
                                            <option
                                                value="{{ $row->id }}" {{ (!empty($result) && $result->school==$row->id)?'selected':'' }}>{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @php
                                $array=[
    1=>'پیش دبستانی',
    2=>'اول',
    3=>'دوم',
    4=>'سوم',
    5=>'چهارم',
    6=>'پنجم',
    7=>'ششم',
    10=>'نهم',
    11=>'دهم',
]
                            @endphp
                            @if(!empty($result) &&  $result->panel_type==2)
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>انتخاب پایه</label>
                                        <select multiple="multiple"
                                                class="js-data-example-ajax select2 levels"
                                                name="levels[]">
                                            @foreach($array as $key=>$value)
                                                <option value="{{ $key }}" {{ (request()->query('levels')==$key)?'selected':'' }} {{ (in_array($key,json_decode($result->levels)))?'selected':'' }}>{{ $value }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            @else
                                <div class="col-md-2 d-none">
                                    <div class="form-group">
                                        <label>انتخاب پایه</label>
                                        <select multiple="multiple"
                                                class="js-data-example-ajax select2 levels"
                                                name="levels[]">
                                            @foreach($array as $key=>$value)
                                                <option value="{{ $key }}" {{ (request()->query('levels')==$key)?'selected':'' }}>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>رمزعبور پنل</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>تکرار رمزعبور پنل</label>
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="button" class="btn btn-success ajaxStore">ثبت</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.js-data-example-ajax').select2({
                width: '100%'
            });
            $(document).on('change', '.panel-type', function () {
                var val = $(this).val();
                if (val == 2) {
                    $(".levels").closest('.col-md-2').removeClass('d-none');
                } else {
                    $(".levels").closest('.col-md-2').addClass('d-none');
                }
            })
        })
    </script>
@endsection
