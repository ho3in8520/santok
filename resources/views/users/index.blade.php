@extends('master_page')
@section('title_browser')
    لیست کاربران
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0"> لیست کاربران</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">نام و نام خانوادگی</th>
                                <th scope="col">کدملی</th>
                                <th scope="col">شماره تماس</th>
                                <th scope="col">عنوان کاربر</th>
                                <th scope="col">عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($users->total() >0)
                                @foreach($users as $item)
                                    <tr>
                                        <td>{{ index($users,$loop) }}</td>
                                        <td>{{ $item->first_name.' '.$item->last_name }}</td>
                                        <td>{{ $item->national_code }}</td>
                                        <td>{{ $item->mobile }}</td>
                                        <td>
                                            @php
                                                $array=[1=>'مدیرسامانه',2=>'کارشناس',3=>'مدیر مدرسه']
                                            @endphp
                                            {{ $array[$item->panel_type] }}
                                        </td>
                                        <td>
                                            <a href="{{ route('users.edit',$item->id) }}" class="btn btn-info btn-sm"
                                               title="ویرایش"><i class="fa fa-pencil-alt"></i> </a>
                                            <a data-key="{{ $item->id }}"
                                               class="btn btn-danger btn-sm delete text-white pointer"
                                               title="حذف"><i class="fa fa-trash"></i> </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <th colspan="4" class="text-center">موردی یافت نشد</th>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        {!! $users->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.delete', function () {
                var id = $(this).data('key');
                swal({
                    title: "اطمینان از انجام عملیات",
                    icon: "warning",
                    buttons: ['انصراف', 'تایید'],
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                type: "DELETE",
                                url: `{{ route('users.destroy') }}`,
                                data: {_token: `{{ csrf_token() }}`, id: id},
                                success: function (response) {
                                    if (response.status == 100) {
                                        swal(response.msg, '', 'success')
                                        setTimeout(function () {
                                            window.location.reload();
                                        }, 3000)
                                    } else {
                                        swal(response.msg, '', 'error')
                                    }
                                }
                            });
                        }
                    });
            })
        })
    </script>
@endsection
