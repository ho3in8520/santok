@extends('master_page')
@section('title_browser')
    لیست درخواست ها
@endsection
@section('style')
    <style>
        .button.disabled {
            opacity: 0.65;
            cursor: not-allowed;
        }

        .button, .button.disabled:hover {
            display: block;
            margin-bottom: 20px;
            text-decoration: none;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            font-family: arial, helvetica, sans-serif;
            padding: 10px 10px 10px 10px;
            text-align: center;
        }
    </style>
@endsection
@section('content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h4 class="page-title m-0"> لیست درخواست ها</h4>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end page-title-box -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <form method="get" action="">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>نام</label>
                                        <input type="text" class="form-control" name="first_name"
                                               value="{{ request()->query('first_name') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>نام خانوادگی</label>
                                        <input type="text" class="form-control" name="last_name"
                                               value="{{ request()->query('last_name') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>نام پدر</label>
                                        <input type="text" class="form-control" name="father_name"
                                               value="{{ request()->query('father_name') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>تاریخ تولد</label>
                                        <input type="text" class="form-control datePicker" name="date_birth"
                                               value="{{ request()->query('date_birth') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>کدملی</label>
                                        <input type="text" maxlength="10" class="form-control" name="national_code"
                                               value="{{ request()->query('national_code') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>مدرسه</label>
                                        <select class="form-control" name="school">
                                            <option value="">انتخاب کنید...</option>
                                            @foreach($schools as $item)
                                                <option
                                                    value="{{ $item->id }}" {{ (request()->query('school')==$item->id)?'selected':'' }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group mt-4">
                                        <button type="button" class="btn btn-info search-ajax">جست و جو</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="summary">{!!getSummary($result) !!}</div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">نام و نام خانوادگی</th>
                                    <th scope="col">نام پدر</th>
                                    <th scope="col">تاریخ تولد</th>
                                    <th scope="col">کدملی</th>
                                    <th scope="col">پایه</th>
                                    <th scope="col">نام مدرسه</th>
                                    <th scope="col">آدرس</th>
                                                                    <th scope="col">وضعیت</th>
                                    <th scope="col">مدرسه</th>
                                    <th scope="col">عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($result->total() >0)
                                    @foreach($result as $item)
                                        <tr>
                                            <td>{{ index($result,$loop) }}</td>
                                            <td>{{ $item->student->first_name.' '.$item->student->last_name }}</td>
                                            <td>{{ $item->student->father_name }}</td>
                                            <td>{{ $item->student->date_birth }}</td>
                                            <td>{{ $item->student->national_code  }}</td>
                                            <td>{{ getLevels($item->levels)  }}</td>
                                            <td>{{ $item->school->name  }}</td>
                                            <td>{{ $item->student->fullAddress  }}</td>
                                                                                    <td>
                                                                                        @if($item->status==0)
                                                                                            <span class="badge badge-warning">درحال بررسی</span>
                                                                                        @elseif($item->status==1)
                                                                                            <span class="badge badge-success">تایید شده</span>
                                                                                        @else
                                                                                            <span class="badge badge-danger">رد شده</span>
                                                                                        @endif
                                                                                    </td>
                                            <td>
                                                @if(empty($item->expert_school_id) && $item->levels!=11)
                                                    <select class="form-control school" data-key="{{ $item->id }}">
                                                        <option value="">انتخاب کنید...</option>
                                                        @foreach($schools as $row)
                                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                        @endforeach
                                                    </select>
                                                @elseif(empty($item->expert_school_id) && $item->levels==11)
                                                    <p class="text-center">-------</p>
                                                @else
                                                    {{ !empty($item->expertSchool)?' مدرسه '.$item->expertSchool->name :'' }} {{ (!empty($item->expert_major))?'  رشته تحصیلی '.$item->expert_major:'' }}
                                                @endif
                                            </td>
                                            <td>
                                                @if(empty($item->expert_school_id) && $item->levels!=11)
                                                    <button type="button" class="btn btn-success btn-sm accept text-white"
                                                            disabled><i
                                                            class="fa fa-check"></i></button>
                                                @elseif(empty($item->expert_school_id) && $item->levels==11)
                                                    <button type="button" data-key="{{ $item->id }}"
                                                            class="btn btn-primary btn-sm information_major text-white"><i
                                                            class="fa fa-list"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <th colspan="9" class="text-center">موردی یافت نشد</th>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $result->appends(request()->query())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Modal -->
    <div class="modal modal-major fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">اطلاعات رشته تحصیلی دانش آموز</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ route('student.school.major.save') }}">
                    @csrf
                    <div class="modal-body modal-body-major">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                        <button type="button" class="btn btn-primary ajaxStore">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="{{ asset('general/function/popup.js') }}"></script>
    <script>
        $(document).on("change", ".school", function () {
            var val = $(this).val();
            if (val != '') {
                $(this).closest('tr').find('button').removeAttr('disabled')
            } else {
                $(this).closest('tr').find('button').attr('disabled', 'disabled')
            }
        });

        $(document).on("click", ".accept", function () {
            var id = $(this).closest('tr').find('select').data('key')
            var val = $(this).closest('tr').find('select').val()
            $.post(`{{ route("expert.student.school.accept") }}`, {
                '_token': `{{ csrf_token() }}`,
                'id': id,
                'val': val
            }, function (response) {
                if (response.status == 100) {
                    swal('موفق', response.msg, "success")
                    setTimeout(function () {
                        window.location.reload()
                    }, 2000)
                } else {
                    swal('خطا', response.msg, "error")
                }
            })
        })

        $(document).on("click", ".information_major", function () {
            var id = $(this).data('key')
            $.get(`{{ route("student.school.major.information") }}`, {
                'id': id,
            }, function (response) {
                $(".modal-body-major").html(response);
                $(".modal-major").modal("show")
            })
        })

        $(document).on("change", "select[name='school']", function () {
            var id = $(this).val();
            $.get(`{{ route("student.school.major.school") }}`, {
                'id': id,
            }, function (response) {
                $("select[name='major']").html(response);
            })
        })

    </script>
@endsection
