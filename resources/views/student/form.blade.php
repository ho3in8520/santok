@extends('master_page')
@section('title_browser')
    دانش آموز جدید
@endsection
@section('style')
    <style>

        #deleteImage {
            position: relative;
        }

        #deleteImageUpload {
            font-size: 35px;
            margin-left: 10px;
            z-index: 1;
            cursor: pointer;
            position: absolute;
        }

        .preview {
            overflow: hidden;
            width: 160px;
            height: 160px;
            margin: 10px;
            border: 1px solid red;
            direction: ltr !important;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0">دانش آموز جدید</h4>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    @if(isset($student))
                        <livewire:student.form :states="$states" :student="$student" :cities="$cities" :avatar="$avatar"/>
                    @else
                        <livewire:student.form :states="$states"/>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

        $(document).on('click', '#blah', function () {
            $("#imgInp").click();
        });

    </script>
@endsection
