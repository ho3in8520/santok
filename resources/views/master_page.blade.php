<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title_browser')</title>
    <meta content="Admin Dashboard" name="description"/>
    <meta content="ThemeDesign" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="shortcut icon" href="assets/images/favicon.ico">

    @include("layouts.header")
    <livewire:styles/>
    @yield('style')
</head>


<body class="fixed-left">

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>

<!-- Begin page -->
<div id="wrapper">

    <!-- ========== Left Sidebar Start ========== -->
@include("layouts.right_menu")
<!-- Left Sidebar End -->

    <!-- Start right Content here -->

    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <!-- Top Bar Start -->
        @include("layouts.top_menu")
        <!-- Top Bar End -->

            <div class="page-content-wrapper">

                <div class="container-fluid">

                    @yield('content')

                </div><!-- container fluid -->

            </div> <!-- Page content Wrapper -->

        </div> <!-- content -->

        <footer class="footer">
            <!--© 1398 زینزر <span class="d-none d-md-inline-block"> - طراحی با <i class="mdi mdi-heart text-danger"></i> فارسی سازی توسط جعفر عباسی.</span>-->
        </footer>

    </div>
    <!-- End Right content here -->

</div>
<!-- END wrapper -->
@include("layouts.footer")
<livewire:scripts/>
<script src="{{ asset('general/function/liveWire.js') }}"></script>
<script>
    $(document).on("change", "select.state", function () {
        var id = $(this).val();

        $.get("{{route('get-cities')}}", {id: id}, function (data) {
            $("select.city").html(JSON.parse(data));
        })
    });
</script>
@yield('script')
</body>
</html>
