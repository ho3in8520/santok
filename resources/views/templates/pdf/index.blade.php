<style>
    body {
        font-family: 'fa', sans-serif;
        direction: rtl;
    }

    table {
        width: 100% !important;
    }

    table, th, td {
        border-collapse: collapse;
        border: 1px solid black;
        padding: 5px;
    }
</style>
<body>
@if(count($data))
    <div style="overflow: hidden !important;">
        <div style="float: left !important"> مدیریت آموزش و پرورش شهرستان تیران و کرون
            سامانه نام نویسی دانش آموزان شهرستان تیران و کرون
            سنتوک
        </div>
    </div>
    <hr style="margin-top: 5px;">
    <table class="table table-bordered table-responsive">
        <thead>
        <tr>
            @foreach($data[0] as $key=>$row)
                <th>{{$key}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)
            <tr>
                @foreach($row as $key=>$item)
                    <td>{{ $item }}</td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p>موردی یافت نشد.</p>
@endif
</body>
