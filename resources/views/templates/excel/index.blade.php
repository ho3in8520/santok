
<table>
    <thead>
    <tr>
        @foreach($data[0] as $key=>$row)
        <th>{{$key}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            @foreach($row as $key=>$item)
            <td>{{ $item }}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
