<link href="{{ asset('theme/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset("theme/assets/css/icons.css") }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/assets/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('theme/plugins/alertify/css/alertify.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('general/select2/select2.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('general/datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
