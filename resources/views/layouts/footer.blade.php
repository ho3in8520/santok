<!-- jQuery  -->
<script src="{{ asset('theme/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('theme/assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('theme/assets/js/modernizr.min.js') }}"></script>
<script src="{{ asset('theme/assets/js/detect.js') }}"></script>
<script src="{{ asset('theme/assets/js/fastclick.js') }}"></script>
<script src="{{ asset('theme/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('theme/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('theme/assets/js/waves.js') }}"></script>
<script src="{{ asset('theme/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('theme/assets/js/jquery.scrollTo.min.js') }}"></script>

<!-- App js -->
<script src="{{ asset('theme/assets/js/app.js') }}"></script>
<script src="{{ asset('theme/plugins/alertify/js/alertify.js') }}"></script>
<script src="{{ asset('general/select2/select2.min.js') }}"></script>
<script src="{{ asset('theme/assets/pages/alertify-init.js') }}"></script>
<script src="{{ asset('general/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('general/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('general/datepicker/bootstrap-datepicker.fa.min.js') }}"></script>
<script src="{{ asset('general/function/project.js') }}"></script>
<script src="{{ asset('general/function/func.js') }}"></script>

