<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="mdi mdi-close"></i>
    </button>

    <div class="left-side-logo d-block d-lg-none">
        <div class="text-center">

            <a href="index-2.html" class="logo"><img src="assets/images/logo_dark.png" height="20" alt="logo"></a>
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">اصلی</li>

                <li>
                    <a href="{{ route('dashboard') }}" class="waves-effect">
                        <i class="dripicons-home"></i>
                        <span> داشبورد <span class="badge badge-success badge-pill float-right">3</span></span>
                    </a>
                </li>

                {{--                <li>--}}
                {{--                    <a href="{{ route('dashboard') }}" class="waves-effect">--}}
                {{--                        <i class="dripicons-home"></i>--}}
                {{--                        <span> ثبت مدرسه </span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                @can('schools')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-briefcase"></i>
                            <span> مدارس </span> <span class="menu-arrow float-right"><i
                                    class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            @can('school-list')
                                <li><a href="{{ route('school.index') }}">لیست مدارس</a></li>
                            @endcan
                            @can('school-register')
                                <li><a href="{{ route('school.create') }}">ثبت مدارس</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('students')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-briefcase"></i> <span> دانش آموز </span>
                            <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            @can('student-list')
                                <li><a href="{{ route('student.index') }}">لیست دانش آموزان</a></li>
                            @endcan
                            @can('student-register')
                                <li><a href="{{ route('student.create') }}">ثبت دانش آموز</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('academic-years-manager')
                    <li>
                        <a href="{{ route('academic.years') }}" class="waves-effect">
                            <i class="dripicons-briefcase"></i>
                            <span>سال تحصیلی</span>
                        </a>
                    </li>
                @endcan
                @can('user-group-manager')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-briefcase"></i> <span> گروه های کاربری </span>
                            <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            @can('role-list-group')
                            <li><a href="{{ route('roles.index') }}">لیست گروه ها</a></li>
                            @endcan
                                @can('role-register-group')
                            <li><a href="{{ route('roles.create') }}">ثبت گروه جدید</a></li>
                                @endcan
                                @can('list-users')
                            <li><a href="{{ route('users.index') }}">لیست کاربران</a></li>
                                @endcan
                                @can('register-user')
                            <li><a href="{{ route('users.create') }}">ثبت کاربر جدید</a></li>
                                @endcan
                        </ul>
                    </li>
                @endcan
                @can('academic-student')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-briefcase"></i> <span> مدیریت مدرسه </span>
                            <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            @can('register-student-school')
                                <li><a href="{{ route('student.school.search') }}">کلاس بندی دانش آموز</a></li>
                            @endcan
                            @can('list-request-student-school')
                                <li><a href="{{ route('student.school.index') }}">لیست درخواست ها</a></li>
                            @endcan
                                @can('student-inquiry')
                                <li><a href="{{ route('student.school.inquiry') }}">استعلام دانش آموز</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('expert')
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="dripicons-briefcase"></i> <span> کارشناس </span>
                            <span class="menu-arrow float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                        <ul class="list-unstyled">
                            @can('expert-list-request')
                                <li><a href="{{ route('expert.student.school.index') }}">لیست درخواست ها</a></li>
                            @endcan
                                @can('reporting')
                                <li><a href="{{ route('reporting.students') }}">گزارش گیری</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>
