@extends('auth.master_page')
@section('title_browser')
    ورود
@endsection
@section('content')
    <div class="row align-items-center">
        <div class="col-lg-6 offset-lg-1">
            <img src="{{ asset('default-image/auth.jpeg') }}" style="width: 100%; border-radius:40px;">
            {{--<div class="text-left">
                <div>
                    <a href="index-2.html" class="logo logo-admin"><img src="{{ asset('theme/assets/images/logo_dark.png') }}" height="28"
                                                                        alt="logo"></a>
                </div>
                <h5 class="font-14 text-muted mb-4">زینزر - داشبورد ادمین بوت استرپ 4 ریسپانسیو</h5>
                <p class="text-muted mb-4">یک قالب مدیریتی حرفه ای برای انواع ادمین هایی که میخواهد شرکت و یا تجارت و
                    مشتریان خود را اداره کنند.</p>

                <h5 class="font-14 text-muted mb-4">مقررات :</h5>
                <div>
                    <p><i class="mdi mdi-arrow-right text-primary mr-2"></i>ستفاده از طراحان گرافیک است..</p>
                    <p><i class="mdi mdi-arrow-right text-primary mr-2"></i>لورم ایپسوم متن ساختگی با تولید سادگی
                        نامفهوم از صنعت چاپ و با ا.</p>
                    <p><i class="mdi mdi-arrow-right text-primary mr-2"></i>لورم ایپسوم متن ساختگی با تولید سادگی
                        نامفهوم از صنعت چاپ و با ا .</p>
                </div>
            </div>--}}
        </div>
        <div class="col-lg-5">
            <div class="card mb-0">
                <livewire:auth.login/>
            </div>
        </div>
    </div>
@endsection
