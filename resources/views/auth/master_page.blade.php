<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('title_browser')</title>
    <meta content="Admin Dashboard" name="description"/>
    <meta content="ThemeDesign" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="shortcut icon" href="assets/images/favicon.ico">
    {!! htmlScriptTagJsApi() !!}
    @include('layouts.header')
    <livewire:styles/>
    @yield('style')
</head>


<body class="fixed-left">

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>


<div class="account-pages">

    <div class="container">
    @yield('content')
    <!-- end row -->
    </div>
</div>
@include('layouts.footer')
<livewire:scripts/>
<script src="{{ asset('general/function/liveWire.js') }}"></script>
@yield('script')
</body>
</html>
