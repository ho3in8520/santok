@extends('master_page')
@section('title_browser')
    گزارش گیری
@endsection
@section('style')
    <link href="{{ asset('general/map/leaflet.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <h4 class="page-title m-0"> گزارش گیری</h4>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end page-title-box -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <form method="get" action="{{ route('reporting.students') }}">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>نام</label>
                                        <input type="text" class="form-control" name="first_name"
                                               value="{{ request()->query('first_name') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>نام خانوادگی</label>
                                        <input type="text" class="form-control" name="last_name"
                                               value="{{ request()->query('last_name') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>نام پدر</label>
                                        <input type="text" class="form-control" name="father_name"
                                               value="{{ request()->query('father_name') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>تاریخ تولد</label>
                                        <input type="text" class="form-control datePicker" name="date_birth"
                                               value="{{ request()->query('date_birth') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>کدملی</label>
                                        <input type="text" maxlength="10" class="form-control" name="national_code"
                                               value="{{ request()->query('national_code') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>تلفن ثابت</label>
                                        <input type="text" maxlength="11" class="form-control" name="phone"
                                               value="{{ request()->query('phone') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>تلفن همراه</label>
                                        <input type="text" maxlength="11" class="form-control" name="mobile"
                                               value="{{ request()->query('mobile') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>استان</label>
                                        <select class="form-control state" name="state">
                                            <option value="">انتخاب کنید...</option>
                                            @foreach($states as $item)
                                                <option
                                                    value="{{ $item->id }}" {{ (request()->query('state')==$item->id)?'selected':'' }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>شهر</label>
                                        <select class="form-control city" name="city">
                                            <option value="">انتخاب کنید...</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>پایه</label>
                                        <select class="form-control" name="levels">
                                            <option value="" {{ (request()->query('levels')=='')?'selected':'' }}>انتخاب
                                                کنید...
                                            </option>
                                            <option value="1" {{ (request()->query('levels')==1)?'selected':'' }}>پیش
                                                دبستانی
                                            </option>
                                            <option value="2" {{ (request()->query('levels')==2)?'selected':'' }}>اول
                                            </option>
                                            <option value="3" {{ (request()->query('levels')==3)?'selected':'' }}>دوم
                                            </option>
                                            <option value="4" {{ (request()->query('levels')==4)?'selected':'' }}>سوم
                                            </option>
                                            <option value="5" {{ (request()->query('levels')==5)?'selected':'' }}>چهارم
                                            </option>
                                            <option value="6" {{ (request()->query('levels')==6)?'selected':'' }}>پنجم
                                            </option>
                                            <option value="7" {{ (request()->query('levels')==7)?'selected':'' }}>ششم
                                            </option>
                                            <option value="10" {{ (request()->query('levels')==10)?'selected':'' }}>نهم
                                            </option>
                                            <option value="11" {{ (request()->query('levels')==11)?'selected':'' }}>دهم
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>مدرسه</label>
                                        <select class="form-control" name="school">
                                            <option value="">انتخاب کنید...</option>
                                            @foreach($schools as $item)
                                                <option
                                                    value="{{ $item->id }}" {{ (request()->query('school')==$item->id)?'selected':'' }}>{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>تابعیت</label>
                                        <select class="form-control" name="nationality">
                                            <option value="">انتخاب کنید...</option>
                                            <option
                                                value="iran" {{ (request()->query('nationality')=='iran')?'selected':'' }}>
                                                ایرانی
                                            </option>
                                            <option
                                                value="foreign" {{ (request()->query('nationality')=='foreign')?'selected':'' }}>
                                                خارجی
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>رشته تحصیلی</label>
                                        <select class="form-control" name="major">
                                            <option value="">انتخاب کنید...</option>
                                            @foreach(getAllMajors(false) as $item)
                                                <option
                                                    value="{{ $item }}" {{ (request()->query('major')==$item)?'selected':'' }}>{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-info search-ajax">جست و جو</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        @if(!empty($result))
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="summary">{!!getSummary($result) !!}</div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th colspan="10"></th>
                                                <th><a data-export="excel"
                                                       class="export btn btn-success btn-sm text-white pointer {{ ($result->total() == 0)?'disabled':'' }}"><i
                                                            class="fa fa-file-excel"></i> خروجی EXCEL</a></th>
                                                <th><a data-export="pdf"
                                                       class="export btn btn-primary btn-sm text-white pointer {{ ($result->total() == 0)?'disabled':'' }}"><i
                                                            class="fa fa-file-pdf"></i> خروجی PDF</a></th>
                                            </tr>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">نام و نام خانوادگی</th>
                                                <th scope="col">نام پدر</th>
                                                <th scope="col">تاریخ تولد</th>
                                                <th scope="col">کدملی</th>
                                                <th scope="col">پایه</th>
                                                <th scope="col">نام مدرسه ثبت نام کننده</th>
                                                <th scope="col">آدرس</th>
                                                <th scope="col">تابعیت</th>
                                                <th scope="col">محل تحصیل 1404-1403</th>
                                                <th scope="col">رشته تحصیلی</th>
                                                <th scope="col">عملیات</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($result->total() >0)
                                                @php
                                                    $nationality=['foreign'=>'خارجی','iran'=>'ایرانی']
                                                @endphp
                                                @foreach($result as $item)
                                                    <tr>
                                                        <td>{{ index($result,$loop) }}</td>
                                                        <td>{{ $item->student->first_name.' '.$item->student->last_name }}</td>
                                                        <td>{{ $item->student->father_name }}</td>
                                                        <td>{{ $item->student->date_birth }}</td>
                                                        <td>{{ $item->student->national_code  }}</td>
                                                        <td>{{ getLevels($item->levels)  }}</td>
                                                        <td>{{ $item->school->name  }}</td>
                                                        <td>{{ $item->student->fullAddress  }}</td>
                                                        <td>{{ $nationality[$item->student->nationality]  }}</td>
                                                        <td>
                                                            {{ !empty($item->expertSchool)?$item->expertSchool->name:'مشخص نشده' }}
                                                        </td>
                                                        <td>{{ ($item->expert_major)?$item->expert_major:'مشخص نشده'  }}</td>
                                                        <td>
                                                            @if($item->expert_major)
                                                                <a title="ویرایش کلاس بندی دانش آموز"
                                                                   data-key="{{ $item->id }}"
                                                                   class="btn btn-info btn-sm edit text-white"><i
                                                                        class="fa fa-pencil-alt"></i></a></td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <th colspan="12" class="text-center">موردی یافت نشد</th>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        {!! $result->appends(request()->query())->render() !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="modal modal-edit fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ویرایش پایه تحصیلی دانش آموز</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="{{ route('student.school.update.student') }}">
                    @csrf
                    <div class="modal-body modal-body-edit">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                        <button type="button" class="btn btn-primary ajaxStore">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('general/function/popup.js') }}"></script>
    <script>
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPosts(page);
                }
            }
        });
        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href'));
                e.preventDefault();
            });
            $(document).on("change", ".levels-edit", function () {
                var val = $(this).val();
                if (val >= 10) {
                    $(".major-edit").show()
                } else {
                    $(".major-edit").hide()
                }
            });
            $(document).on("click", ".edit ", function () {
                var id = $(this).data('key')
                $.get(`{{ route("student.school.edit.student") }}`, {
                    'id': id,
                }, function (response) {
                    $(".modal-body-edit").html(response);
                    $(".modal-edit").modal("show")
                })
            })
            $(document).on("change", "select[name='school']", function () {
                var id = $(this).val();
                $.get(`{{ route("student.school.major.school") }}`, {
                    'id': id,
                }, function (response) {
                    $("select[name='major']").html(response);
                })
            })
        });

        function getPosts(page) {
            console.log(page)
            $.ajax({
                url: page,
                dataType: 'json',
            }).done(function (data) {
                $('#basic-form-layouts').html(data.FormatHtml);
                // location.hash = page;
            }).fail(function () {
                alert('Posts could not be loaded.');
            });
        }
    </script>
@endsection
