@extends("master_page")
@section('title_browser')
    داشبورد
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h4 class="page-title m-0"></h4>
                    </div>
                    <div class="col-md-4">
                        <div class="float-right d-none d-md-block">
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card bg-primary mini-stat text-white">
                <div class="p-3 mini-stat-desc">
                    <div class="clearfix">
                        <h6 class="text-uppercase mt-0 float-left text-white-50">تعداد کاربران کارشناس</h6>
                        <h4 class="mb-3 mt-0 float-right">{{ $result['count_expert'] }}</h4>
                    </div>
                </div>
                <div class="p-3">
                    <div class="float-right">
                        <a href="#" class="text-white-50"><i class="mdi mdi-cube-outline h5"></i></a>
                    </div>
                    {{--                    <p class="font-14 m-0">آخر : 1447</p>--}}
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card bg-info mini-stat text-white">
                <div class="p-3 mini-stat-desc">
                    <div class="clearfix">
                        <h6 class="text-uppercase mt-0 float-left text-white-50">تعداد کاربران مدیر مدرسه</h6>
                        <h4 class="mb-3 mt-0 float-right">{{ $result['count_headmaster'] }}</h4>
                    </div>
                    <div>
                        {{--                        <span class="badge badge-light text-danger"> -29% </span> <span class="ml-2">از دوره قبلی</span>--}}
                    </div>
                </div>
                <div class="p-3">
                    <div class="float-right">
                        <a href="#" class="text-white-50"><i class="mdi mdi-buffer h5"></i></a>
                    </div>
                    {{--                    <p class="font-14 m-0">آخر : 46,785 تومان</p>--}}
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-pink mini-stat text-white">
                <div class="p-3 mini-stat-desc">
                    <div class="clearfix">
                        <h6 class="text-uppercase mt-0 float-left text-white-50">تعداد مدرسه</h6>
                        <h4 class="mb-3 mt-0 float-right">{{ $result['count_school'] }}</h4>
                    </div>
                    <div>
                        {{--                        <span class="badge badge-light text-primary"> 0% </span> <span class="ml-2">از دوره قبلی</span>--}}
                    </div>
                </div>
                <div class="p-3">
                    <div class="float-right">
                        <a href="#" class="text-white-50"><i class="mdi mdi-tag-text-outline h5"></i></a>
                    </div>
                    {{--                    <p class="font-14 m-0">آخر : 15.8</p>--}}
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card bg-success mini-stat text-white">
                <div class="p-3 mini-stat-desc">
                    <div class="clearfix">
                        <h6 class="text-uppercase mt-0 float-left text-white-50">تعداد درخواست های ثبت نام </h6>
                        <h4 class="mb-3 mt-0 float-right">{{ $result['count_request_register_school'] }}</h4>
                    </div>
                    <div>
{{--                        <span class="badge badge-light text-info"> +89% </span> <span class="ml-2">از دوره قبلی</span>--}}
                    </div>
                </div>
                <div class="p-3">
                    <div class="float-right">
                        <a href="#" class="text-white-50"><i class="mdi mdi-briefcase-check h5"></i></a>
                    </div>
{{--                    <p class="font-14 m-0">آخر : 1776</p>--}}
                </div>
            </div>
        </div>
    </div>
@endsection
