
@php
    $ip =  Request()->ip() == '::1' ? '185.159.152.0' : Request()->ip() ;
    /*$data = Location::get($ip);*/
    $lat = isset($lat) ? $lat : 32.6993973;
    $lng = isset($lng) ? $lng : 51.1522197;

@endphp
@section('lat')
    {{ $lat }}
@endsection
@section('lng')
    {{ $lng }}
@endsection
<link rel="stylesheet" href="{{ asset('general/map/leaflet.css') }}"/>
<div id="map" wire:ignore style="width: 500px;height: 300px"></div>
<input type="hidden" name="location_lat" wire:model.defer="lat" id="lat" value="{{ (!empty($latMarker))? $latMarker :'' }}">
<input type="hidden" name="location_lng" wire:model.defer="lon" id="lng" value="{{ (!empty($lngMarker))? $lngMarker :'' }}">
<script src="{{ asset('general/map/leaflet.js') }}"></script>

<script>
    var map = L.map('map').setView([@yield('lat'),@yield('lng')], 9);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(map);


    var theMarker = {};
    theMarker = L.marker([{{ $lat }}, {{ $lng }}], {
        draggable: false,
    }).addTo(map);
    map.on('click', function (e) {
        lat = e.latlng.lat;
        lon = e.latlng.lng;

        if (theMarker != undefined) {
            map.removeLayer(theMarker);
        }
        ;

        //Add a marker to show where you clicked.
        theMarker = L.marker([lat, lon], {
            draggable: false,
        }).addTo(map);

        window.livewire.emit('lat-lon', lat, lon)

        $("#lat").val(lat);
        $("#lng").val(lon);
    });
</script>
