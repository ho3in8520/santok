<?php

return [
    'title-dashboard-page' => 'داشبورد',
    'title-news-list' => 'لیست خبر ها',
    'title-news-page' => 'خبر',
    'title-design-page' => 'طرح ها',
    'title-education-page' => 'آموزش ها'
];
