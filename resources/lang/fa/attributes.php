<?php

return [
    "national_code" => "کد ملی",
    "password" => "رمزعبور",
    "code" => "کد",
    "name" => "نام",
    "phone" => "تلفن",
    "state" => "استان",
    "city" => "شهر",
    "address" => "آدرس",
    "lat" => "نقشه",
    "first_name" => "نام",
    "last_name" => "نام خانوادگی",
    "father_name" => "نام پدر",
    "date_birth" => "تاریخ تولد",
    "mobile" => "موبایل",
    "imageStudent" => "تصویر دانش آموز",
    "year" => "سال",
    "panel_type" => "نوع پنل",
    "role" => "پکیج",
    "school" => "مدرسه",
    "nationality" => "تابعیت",
    "zone" => "منطقه/روستا",
    "street" => "خیابان",
    "plaque" => "پلاک",
    "postal_code" => "کدپستی",
    "ownership" => "مالکیت",
    "major" => "رشته تحصیلی",
    "student.first_name" => "نام",
    "student.last_name" => "نام خانوادگی",
    "student.father_name" => "نام پدر",
    "student.date_birth" => "تاریخ تولد",
    "student.mobile" => "موبایل",
    "student.national_code" => "کد ملی",
    "student.nationality" => "تابعیت",
    "student.city" => "شهر",
    "student.zone" => "منطقه/روستا",
    "student.street" => "خیابان",
];
