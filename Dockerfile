FROM php:7.4-apache

# # Set working directory
WORKDIR /var/www/html

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    build-essential \
    libfreetype6-dev \
    libmcrypt-dev \
    libgd-dev \
    libonig-dev \
    libpng-dev \
    libjpeg-dev \
    libjpeg62-turbo-dev \
    jpegoptim optipng pngquant gifsicle \
    libonig-dev \
    libxml2-dev \
    libbz2-dev \
    libgmp-dev \
    libc-client-dev \
    libkrb5-dev \
    libzip-dev \
    libpng-dev \
    zip \
    unzip \
    gnupg2 \
    tzdata \
    cron \
    logrotate \
    vim

RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg

RUN apt-get update \
 && apt-get install --assume-yes --no-install-recommends --quiet \
    build-essential \
    libmagickwand-dev \
 && apt-get clean all

RUN docker-php-ext-install pdo pdo_mysql mbstring gd gmp zip sockets bcmath exif && a2enmod rewrite
# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copy existing application directory contents
COPY . /var/www/html/santok
COPY ./docker/htaccess /var/www/html/.htaccess

RUN chown www-data:www-data ./* -Rf
RUN chown www-data:www-data ./.* -Rf
RUN chmod 777 santok/storage/*

CMD composer update --working-dir=./santok \
&& apache2-foreground
