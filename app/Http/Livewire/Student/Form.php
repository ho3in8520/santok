<?php

namespace App\Http\Livewire\Student;

use App\Models\City;
use App\Models\Student;
use App\Rules\JalaliDataFormat;
use App\Rules\MobileFormat;
use App\Rules\NationalCodeFormat;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;

class Form extends Component
{
    use WithFileUploads;

    public $imageStudent;
    public $preview;
    public $cities = [];
    public $student = [];
    public $avatar;
    public $state = 4;
    public $city_id = 0;
    public $arrayZones = [
        192 => [
            0 => 'منطقه1',
            1 => 'منطقه2',
            2 => 'منطقه3',
            3 => 'منطقه4',
            4 => 'منطقه5',
            5 => 'منطقه6',
            6 => 'منطقه7',
            7 => 'شهرک الغدیر',
            8 => 'شهرک نشاط',
            9 => 'رزمندگان',
            10 => 'فدک',
            11 => 'صادقیه',
            12 => 'بهارستان',
            13 => 'باهنر',
        ],
        193 => [
            0 => 'رضوانشهر'
        ],
        194 => [
            0 => 'عسگران'
        ],
    ];

    protected $listeners = [
        'datePicker' => 'setDatePicker'
    ];

    public function render()
    {
        if (isset($this->student['city']) && !empty($this->student['city'])) {
            $this->city_id = $this->student['city'];
        }
        $this->cities();
        if (!empty($this->avatar)) {
            $this->preview = $this->avatar->path;
            $pic = explode('/', $this->avatar->path);
            $this->preview = route('get-avatar', [$pic[0], $pic[1]]);
        }

        if (!empty($this->imageStudent)) {
            $this->preview = $this->imageStudent->temporaryUrl();
        }
        return view('livewire.student.form');
    }

    public function submit()
    {
        $this->validate([
            'student.first_name' => 'required',
            'student.last_name' => 'required',
            'student.father_name' => 'required',
            'student.date_birth' => ['nullable', new JalaliDataFormat()],
            'student.national_code' => ['required', 'numeric', 'unique:students,national_code', function ($attr, $value, $fail) {
                if ($this->student['nationality'] == 'iran') {
                    if (!preg_match('/^[0-9]{10}$/', $value))
                        $fail(trans('validation.national_code'));
                    for ($i = 0; $i < 10; $i++)
                        if (preg_match('/^' . $i . '{10}$/', $value))
                            return false;
                    for ($i = 0, $sum = 0; $i < 9; $i++)
                        $sum += ((10 - $i) * intval(substr($value, $i, 1)));
                    $ret = $sum % 11;
                    $parity = intval(substr($value, 9, 1));
                    if (($ret < 2 && $ret == $parity) || ($ret >= 2 && $ret == 11 - $parity))
                        return true;
                    $fail(trans('validation.national_code'));
                }
            }],
            //                'address' => 'required',
            // 'student.phone' => 'required',
            'student.mobile' => ['required', new MobileFormat()],
            //                'state' => 'required',
            'student.city' => 'nullable',
            'student.nationality' => 'required|in:iran,foreign',
            'student.zone' => 'required',
            'student.street' => 'nullable',
            //                'plaque' => 'required|numeric',
            //                'postal_code' => 'required|numeric|digits:10',
            //                'ownership' => 'required|in:personal,tenant',
            //            'imageStudent' => 'required|image|max:1024',
        ]);

        try {
            $school_id = auth()->user()->school;
            $model = Student::create(array_merge($this->student, ['school_id' => $school_id, 'state' => 4]));

            if ($this->imageStudent) {
                $name = Str::random(10) . '.' . $this->imageStudent->getClientOriginalExtension();
                $path = $this->imageStudent->storeAs('/', auth()->user()->id . '/' . $name, $disk = 'avatars');

                $model->files()->create([
                    'user_id' => auth()->user()->id,
                    'path' => $path,
                    'mime_type' => $this->imageStudent->getMimeType(),
                    'type' => 'avatar'
                ]);
            }
            return $this->emit('swal', ['type' => 'success', 'message' => 'عملیات با موفقیت انجام گردید', 'url' => route('student.index'), 'time' => 3000]);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return $this->emit('swal', ['type' => 'error', 'message' => 'خطا در ثبت اطلاعات']);
        }
    }

    public function update()
    {
        $this->validate([
            'student.first_name' => 'required',
            'student.last_name' => 'required',
            'student.father_name' => 'required',
            'student.date_birth' => ['required', new JalaliDataFormat()],
            'student.national_code' => ['required', 'numeric', function ($attr, $value, $fail) {
                if ($this->student['nationality'] == 'iran') {
                    if (!preg_match('/^[0-9]{10}$/', $value))
                        $fail(trans('validation.national_code'));
                    for ($i = 0; $i < 10; $i++)
                        if (preg_match('/^' . $i . '{10}$/', $value))
                            return false;
                    for ($i = 0, $sum = 0; $i < 9; $i++)
                        $sum += ((10 - $i) * intval(substr($value, $i, 1)));
                    $ret = $sum % 11;
                    $parity = intval(substr($value, 9, 1));
                    if (($ret < 2 && $ret == $parity) || ($ret >= 2 && $ret == 11 - $parity))
                        return true;
                    $fail(trans('validation.national_code'));
                }
            }, 'unique:students,national_code,' . $this->student['id']],
            // 'student.phone' => 'required',
            'student.mobile' => ['required', new MobileFormat()],
            'student.city' => 'required',
            'student.nationality' => 'required|in:iran,foreign',
            'student.zone' => ['required'],
            'student.street' => 'required',
            'imageStudent' => 'nullable|image|max:1024',
        ]);

        try {
            $model = Student::find($this->student['id']);
            $model->update($this->student);

            if ($this->imageStudent) {
                $name = Str::random(10) . '.' . $this->imageStudent->getClientOriginalExtension();

                $path = $this->imageStudent->storeAs('/', auth()->user()->id . '/' . $name, $disk = 'avatars');

                $model->files()->where('type', 'avatar')->updateOrCreate([
                    'user_id' => auth()->user()->id,
                    'type' => 'avatar'
                ], [
                    'path' => $path,
                    'mime_type' => $this->imageStudent->getMimeType(),
                ]);
            }
            return $this->emit('swal', ['type' => 'success', 'message' => 'عملیات با موفقیت انجام گردید', 'url' => route('student.index'), 'time' => 3000]);
        } catch (\Exception $e) {
            return $this->emit('swal', ['type' => 'error', 'message' => 'خطا در ثبت اطلاعات']);
        }
    }

    public function cities()
    {
        $this->cities = City::select(['id', 'name'])->where('parent', $this->state)->where('status', 1)->get();
    }

    public function removePreview()
    {
        $this->imageStudent = '';
        $this->preview = "";
    }

    public function setDatePicker($date)
    {
        $this->student['date_birth'] = $date;
    }

    public function changePreview()
    {
        //        dd($this->imageStudent);
        //        $this->preview=$this->imageStudent->temporaryUrl();
    }

    public function zone($val)
    {
        $this->city_id = $val;
        $this->student['zone'] = null;
    }
}
