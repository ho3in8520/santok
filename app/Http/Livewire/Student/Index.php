<?php

namespace App\Http\Livewire\Student;

use App\Models\AcademicStudent;
use App\Models\Student;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $states;
    public $cities = [];
    public $name;
    public $father_name;
    public $national_code;
    public $state;
    public $city;
    protected $listeners = [
        'deleteStudent' => 'deleteStudent'
    ];

    public function render()
    {
        $students = $this->query();

        return view('livewire.student.index', compact('students'));
    }

    public function query()
    {
        $user = auth()->user();
        $student = Student::select(['*', DB::raw("(concat(first_name,' ',last_name)) as full_name")]);
        if (in_array($user->panel_type, [3])) {
            $student->where('school_id', auth()->user()->school);
        }
        if (!empty($this->name)) {
            $student->whereRaw("(concat(first_name,' ',last_name) like '%$this->name%')");
//            $student->where(function ($q) {
//                $q->where('first_name', 'like', '%' . $this->name . '%')
//                    ->OrWhere('last_name', 'like', '%' . $this->name . '%');
//            });
        }
        if (!empty($this->father_name)) {
            $student->where('father_name', 'like', '%' . $this->father_name . '%');
        }
        if (!empty($this->national_code)) {
            $student->where('national_code', $this->national_code);
        }
        if (!empty($this->state)) {
            $student->where('state', $this->state);
        }
        if (!empty($this->city)) {
            $student->where('city', $this->city);
        }
        return $student = $student->paginate(10);
    }

    public function questionDelete($id)
    {
        return $this->emit('swal', ['question' => 'deleteStudent', 'params' => ['id' => $id], 'title' => 'از حذف دانش آموز مطمئن هستید', 'type' => 'warning']);
    }

     public function deleteStudent($id)
    {
        try {
                Student::where('id', $id['id'])->delete();
            $student = AcademicStudent::where('student_id', $id['id'])->first();
            if ($student) {
                $student->delete();
            }
            return $this->emit('swal', ['type' => 'success', 'message' => 'عملیات با موفقیت انجام گردید', 'url' => route('student.index'), 'time' => 3000]);
        } catch (\Exception $e) {
            return $this->emit('swal', ['type' => 'error', 'message' => 'خطا در ثبت اطلاعات']);
        }
    }
}
