<?php

namespace App\Http\Livewire\School;

use App\Models\City;
use App\Models\School;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $states;
    public $state;
    public $city;
    public $cities = [];
    public $code;
    public $name;
    public $phone;
    public $buttonQuestion;
    protected $listeners = [
        'inactive' => 'question'
    ];


    public function render()
    {
        $schools = $this->query();
        return view('livewire.school.index', compact('schools'));
    }

    public function cities()
    {
        $this->cities = City::select(['id', 'name'])->where('parent', $this->state)->get();
    }

    public function query()
    {
        $schools = School::query();
        if (!empty($this->code)) {
            $schools->where('code', $this->code);
        }
        if (!empty($this->name)) {
            $schools->where('name', 'like', '%' . $this->name . '%');
        }
        if (!empty($this->phone)) {
            $schools->where('phone', $this->phone);
        }
        if (!empty($this->state)) {
            $schools->where('state', $this->state);
        }
        if (!empty($this->city)) {
            $schools->where('city', $this->city);
        }

        return $schools = $schools->paginate(10);

    }

    public function swal($id)
    {
        return $this->emit('swal', ['type' => 'warning', 'title' => 'اطمینان از انجام دارید؟', 'question' => 'inactive', 'params' => ['id' => $id]]);
    }

    public function question($data)
    {
        $model = School::find($data['id']);
        $model->status = ($model->status == 1) ? 0 : 1;
        if ($model->save())
            return $this->emit('swal', ['type' => 'success', 'title' => trans('message.success')]);
        return $this->emit('swal', ['type' => 'error', 'title' => trans('message.fail')]);
    }
}
