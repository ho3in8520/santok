<?php

namespace App\Http\Livewire\School;

use App\Models\City;
use App\Models\School;
use Livewire\Component;

class Form extends Component
{
    public $code;
    public $name;
    public $phone;
    public $states;
    public $state;
    public $city;
    public $cities = [];
    public $address;
    public $lat;
    public $lon;
    public $school;
    public $major;
    public $majors = [];
    protected $rules = [
        'code' => 'required|numeric',
        'name' => 'required',
        'phone' => 'required|numeric',
        'state' => 'required|exists:cities,id',
        'city' => 'required|exists:cities,id',
        'address' => 'required',
        'lat' => 'required',
    ];

    protected $listeners = [
        'lat-lon' => 'location',
        'major' => 'setMajor'
    ];

    public function render()
    {
        if (!empty($this->school)) {
            $this->code = $this->school->code;
            $this->name = $this->school->name;
            $this->phone = $this->school->phone;
            $this->state = $this->school->state;
            $this->city = $this->school->city;
            $this->address = $this->school->address;
            $this->lat = $this->school->location_lat;
            $this->lon = $this->school->location_lon;
            $this->majors = ($this->school->major) ? json_decode($this->school->major) : [];
        }
        return view('livewire.school.form');
    }

    public function submit()
    {
        $this->validate();
        if (!empty($this->school)) {
            $model = School::find($this->school->id);
        } else {
            $model = new School();
        }
        $model->code = $this->code;
        $model->name = $this->name;
        $model->phone = $this->phone;
        $model->state = $this->state;
        $model->city = $this->city;
        $model->address = $this->address;
        $model->location_lat = $this->lat;
        $model->location_lon = $this->lon;
        $model->major = ($this->major) ? json_encode($this->major) : (!empty($this->majors)?$this->majors:null) ;
        if ($model->save()) {
            $this->reset(['code', 'name', 'phone', 'state', 'city', 'address', 'lat', 'lon']);
            return $this->emit('swal', ['type' => 'success', 'message' => 'عملیات با موفقیت انجام گردید', 'url' => route('school.index'), 'time' => 3000]);
        } else
            return $this->emit('swal', ['type' => 'error', 'message' => 'خطا در ثبت اطلاعات']);
    }

    public function cities()
    {
        $this->cities = City::select(['id', 'name'])->where('parent', $this->state)->get();
    }

    public function location($lat, $lon)
    {
        $this->lat = $lat;
        $this->lon = $lon;
    }

    public function setMajor($val)
    {
        $this->major = $val;
    }
}
