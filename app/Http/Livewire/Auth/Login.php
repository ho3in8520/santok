<?php

namespace App\Http\Livewire\Auth;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    public $national_code;
    public $password;
    public $recaptcha;

    protected $rules = [
        'national_code' => 'required|numeric',
        'password' => 'required',
        'recaptcha' => 'recaptcha'
    ];
    protected $listeners=['recaptcha'=>'setRecaptcha'];

    public function render()
    {
        return view('livewire.auth.login');
    }

    public function login()
    {
        $this->validate();
        if ($this->attempt())
            return $this->emit('alert', ['type' => 'success', 'message' => trans('auth.login_success'), 'time' => 3000, 'url' => route('dashboard')]);
        return $this->emit('alert', ['type' => 'error', 'message' => trans('auth.login_fail')]);

    }

    protected function attempt()
    {
        return Auth::attempt(['national_code' => $this->national_code, 'password' => $this->password]);
    }

    public function setRecaptcha($val)
    {
        $this->recaptcha=$val;
    }
}
