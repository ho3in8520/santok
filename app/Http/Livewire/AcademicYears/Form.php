<?php

namespace App\Http\Livewire\AcademicYears;

use App\Models\AcademicYear;
use Livewire\Component;
use Livewire\WithPagination;

class Form extends Component
{
    use WithPagination;

    public $year;
    public $yearEdit;
    protected $rules = [
        'year' => 'required'
    ];

    public function render()
    {
        $yearsItem = AcademicYear::query()->paginate(10);
        if ($this->yearEdit) {

            $this->year = $this->yearEdit->year;
        }
        return view('livewire.academic-years.form', compact('yearsItem'));
    }

    public function submit()
    {
        $this->validate();
        if ($this->yearEdit) {
            $model = AcademicYear::find($this->yearEdit->id);
        } else {
            $model = new AcademicYear();
        }
        $model->year = $this->year;
        if ($model->save()) {
            $this->reset(['year']);
            return $this->emit('swal', ['type' => 'success', 'message' => 'عملیات با موفقیت انجام گردید']);
        } else
            return $this->emit('swal', ['type' => 'error', 'message' => 'خطا در ثبت اطلاعات']);
    }
}
