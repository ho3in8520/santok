<?php

namespace App\Http\Controllers;

use App\Models\AcademicStudent;
use App\Models\School;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function dashboard()
    {
        $result=[];
        $result['count_expert']=User::where('panel_type',2)->count();
        $result['count_headmaster']=User::where('panel_type',3)->count();
        $result['count_school']=School::where('status',1)->count();
        $result['count_request_register_school']=AcademicStudent::whereNull('accepted_at')->count();
        return view('dashboard',compact('result'));
    }
}
