<?php

namespace App\Http\Controllers;

use App\Models\AcademicYear;
use Illuminate\Http\Request;

class AcademicYearsController extends Controller
{
    public function show()
    {
        return view('academic_years.form');
    }

    public function edit($id)
    {
        $result=AcademicYear::findOrFail($id);
        return view('academic_years.form',compact('result'));
    }
}
