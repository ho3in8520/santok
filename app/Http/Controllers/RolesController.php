<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    private function permissionsList()
    {
        $array = [
            [
                'name' => 'schools',
                'label' => 'مدارس',
                'childs' => [
                    [
                        'name' => 'school-list',
                        'label' => 'لیست مدارس',
                    ],
                    [
                        'name' => 'school-register',
                        'label' => 'ثبت مدرسه جدید',
                    ],
                ],
            ],
            [
                'name' => 'students',
                'label' => 'دانش آموز',
                'childs' => [
                    [
                        'name' => 'student-list',
                        'label' => 'لیست دانش آموزان',
                    ],
                    [
                        'name' => 'student-register',
                        'label' => 'ثبت دانش آموز جدید',
                    ], [
                        'name' => 'student-edit',
                        'label' => 'ویرایش دانش آموز',
                    ], [
                        'name' => 'student-delete',
                        'label' => 'حذف دانش آموز',
                    ],
                ],
            ],
            [
                'name' => 'academic-years-manager',
                'label' => 'مدیریت سال تحصیلی',
                'childs' => [
                    [
                        'name' => 'academic-years',
                        'label' => 'مدیریت سال تحصیلی',
                    ],
                ],
            ],
            [
                'name' => 'academic-student',
                'label' => 'مدیریت مدرسه',
                'childs' => [
                    [
                        'name' => 'register-student-school',
                        'label' => 'کلاس بندی دانش آموز',
                    ],
                    [
                        'name' => 'list-request-student-school',
                        'label' => 'لیست درخواست ها',
                    ],
                    [
                        'name' => 'student-inquiry',
                        'label' => 'استعلام دانش آموز',
                    ],
                ],
            ],
            [
                'name' => 'expert',
                'label' => 'کارشناس',
                'childs' => [
                    [
                        'name' => 'expert-list-request',
                        'label' => 'لیست درخواست ها',
                    ],
                    [
                        'name' => 'reporting',
                        'label' => 'گزارش گیری',
                    ],

                ],
            ], [
                'name' => 'user-group-manager',
                'label' => 'مدیریت گروه های کاربری',
                'childs' => [
                    [
                        'name' => 'role-list-group',
                        'label' => 'لیست گروه ها',
                    ],
                    [
                        'name' => 'role-register-group',
                        'label' => 'ثبت گروه جدید',
                    ],
                    [
                        'name' => 'list-users',
                        'label' => 'لیست کاربران',
                    ],
                    [
                        'name' => 'register-user',
                        'label' => 'ثبت کاربر جدید',
                    ],
                ],
            ],
        ];
        $this->createPermissions($array, 0);
    }

    private function resetPermissionsAndRoles()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        foreach (config("permission.table_names") as $table) {
            DB::table($table)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return true;
    }

    private function createPermissions($array, $parent)
    {
        foreach ($array as $permission) {
            $perm = Permission::where("name", $permission["name"])->first();
            if (!$perm) {
                $perm = Permission::create(['name' => $permission['name'], 'label' => $permission['label'], 'guard_name' => "web", 'parent' => $parent]);
            }
            if (isset($permission["childs"])) {
                $this->createPermissions($permission["childs"], $perm->id);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $this->resetPermissionsAndRoles();
        $this->permissionsList();
        $roles = Role::orderBy('id', 'DESC')->paginate(10);
        return view('roles.index', compact('roles'));
    }

    private function panelColorArray($lvl)
    {
        $array = [
            1 => "primary",
            2 => "success",
            3 => "info",
        ];

        return $array[$lvl];
    }

    private function buildPermissionsHtml($permission, $parent = 0, $lvl = 0, $rolePermissions = array())
    {
        $lvl++;
        $str = "";
        foreach ($permission as $value):
            if (isset($value['children'])) {
                $str .= '<div class="panel panel-' . $this->panelColorArray($lvl) . '">
                        <div class="panel-heading px-2">
                            <h5 class="panel-title"> <label class="text-white"><input ' . ((in_array($value['id'],
                        $rolePermissions)) ? "checked" : "") . ' class="parent" type="checkbox" data-parent-id="' . $parent . '" data-id="' . $value['id'] . '" name="permission[]" value="' . $value['id'] . '">
                                    ' . $value['label'] . '</label></h5>
                        </div>
                        <div class="panel-body p-3 border-grey border mb-3">';
                if (isset($value["children"])) {
                    $str .= $this->buildPermissionsHtml($value["children"], $value['id'], $lvl, $rolePermissions);
                }
                $str .= '</div></div>';
            } else {
                $str .= '<label class="text-black pl-3"><input ' . ((in_array($value['id'],
                        $rolePermissions)) ? "checked" : "") . ' class="child" type="checkbox" data-parent-id="' . $parent . '" data-id="' . $value['id'] . '" name="permission[]" value="' . $value['id'] . '">
                                    ' . $value['label'] . '</label>';
            }
        endforeach;

        return $str;
    }

    private function buildTree(array $elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::get()->toArray();
        $permission = $this->buildPermissionsHtml($this->buildTree($permission, 0));

        return view('roles.form', compact('permission'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles,name',
        ]);

        DB::beginTransaction();
        try {
            if ($role = Role::create(['description' => $request->input('description'), 'name' => $request->input('name'), 'guard_name' => "web"])) {
                $role->syncPermissions($request->input('permission'));
                DB::commit();

                return response()->json([
                    'status' => 100,
                    'timer' => 500,
                    'url' => route('roles.index'),
                    'msg' => trans('message.success'),
                ]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
        }

        return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=",
            "permissions.id")
            ->where("role_has_permissions.role_id", $id)
            ->get();


        return view('admin::roles.show', compact('role', 'rolePermissions'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get()->toArray();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();
        $permission = $this->buildPermissionsHtml($this->buildTree($permission, 0), 0, 0, $rolePermissions);

        return view('roles.form', compact('permission', 'role'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:roles,name,' . $id,
        ]);

        DB::beginTransaction();
        try {
            $role = Role::find($id);
            $role->name = request('name');
            $role->save();
            $role->syncPermissions($request->input('permission'));
            DB::commit();

            return response()->json([
                'status' => 100,
                'timer' => 500,
                'url' => route('roles.index'),
                'msg' => trans('message.success'),
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
        }
        return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $model = Role::find($request->id);
        if (!$model->users) {
            $model->delete();
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید']);
        }
        return response()->json(['status' => 500, 'msg' => 'به دلیل اطلاعات وابسته امکان حذف وجود ندارد']);
    }
}
