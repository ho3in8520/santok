<?php

namespace App\Http\Controllers;

use App\Models\AcademicStudent;
use App\Models\AcademicYear;
use App\Models\School;
use App\Models\Student;
use App\Rules\NationalCodeFormat;
use Illuminate\Http\Request;

class StudentSchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = AcademicStudent::where('school_id', auth()->user()->school)
            ->with(['student', 'school', 'academicYear'])->paginate(10);
        return view('student_school.index', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        Student::findOrFail($id);
        $academic_years = AcademicYear::all();
        $majors = School::majorsSelect();
        return view('student_school.form', compact('id', 'academic_years', 'majors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'school' => 'required|exists:schools,id',
            'student' => 'required|exists:students,id',
            'academic_years' => 'required|exists:academic_years,id',
            'levels' => 'required|in:1,2,3,4,5,6,7,8,10,11',
             'private_schools' => 'required_if:levels,==,11',
            // 'first_priority.*' => 'required_if:levels,==,10',
            // 'second_priority.*' => 'required_if:levels,==,10',
            // 'third_priority.*' => 'required_if:levels,==,10',
             'interest.*' => 'required_if:levels,==,11',
             'corrected' => 'required_if:levels,==,11|numeric',
        ], [
            'first_priority.*.required_if' => 'این فیلد الزامی است',
            'second_priority.*.required_if' => 'این فیلد الزامی است',
            'third_priority.*.required_if' => 'این فیلد الزامی است',
            'interest.*.required_if' => 'این فیلد الزامی است',
            'corrected.required_if' => 'این فیلد الزامی است',
            'private_schools.required_if' => 'این فیلد الزامی است',
        ]);
        $check = AcademicStudent::where('student_id', $request->student)->where('academic_year_id', $request->academic_years)->first();
        if ($check) {
            return response()->json(['status' => 500, 'msg' => 'دانش آموز در سال تحصیلی مورد نظر یک بار ثبت نام شده است']);
        }
        $major = '';
        if ($request->levels == 11) {
            $major = json_encode([
                'first_priority' => $request->first_priority,
                'second_priority' => $request->second_priority,
                'third_priority' => $request->third_priority,
                'interest' => $request->interest,
                'corrected' => $request->corrected,
                'private_schools' => $request->private_schools,
            ]);
        }
        $model = new AcademicStudent();
        $model->student_id = $request->student;
        $model->academic_year_id = $request->academic_years;
        $model->school_id = $request->school;
        $model->levels = $request->levels;
        $model->status = 0;
        $model->information_major = (!empty($major)) ? $major : null;
        if ($model->save())
            return response()->json([
                'status' => 100,
                'timer' => 500,
                'url' => route('student.school.index'),
                'msg' => trans('message.success'),
            ]);
        return response()->json(['status' => 500, 'msg' => trans('message.fail')]);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search()
    {
        return view('student_school.search');
    }

    public function searchResponse(Request $request)
    {
        $request->validate([
            'national_code' => ['required', function ($attr, $val, $fail) {
                $model = Student::where('national_code', $val)->first();
                if (!$model) {
                    $fail('چنین دانش آموزی داخل سامانه ثبت نگردیده است');
                }
            }]
        ]);
        $model = Student::where('national_code', $request->national_code)->first();
        return response()->json(['status' => 100, 'msg' => 'عملیات موفق', 'url' => route('student.school.create', $model->id)]);
    }

    public function inquiry(Request $request)
    {
        $result = [];
        return view('student_school.inquiry', compact('result'));
    }

    public function inquiryResponse(Request $request)
    {
        $request->validate([
            'national_code' => ['required', new NationalCodeFormat()]
        ]);
        $result = Student::where('national_code', $request->national_code)->whereHas('academicStudent')
            ->with(['academicStudent' => function ($q) {
                $q->orderBy('id', 'desc')->limit(1);
            }])->first();
        if (!$result)
            return response()->json(['status' => 300, 'msg' => 'موردی یافت نشد']);
        return view('student_school.inquiry_table', compact('result'));
    }
}
