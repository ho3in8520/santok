<?php

namespace App\Http\Controllers;

use App\Models\PanelType;
use App\Models\School;
use App\Models\User;
use App\Rules\JalaliDataFormat;
use App\Rules\MobileFormat;
use App\Rules\NationalCodeFormat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $panel_types = PanelType::whereIn('id', [2, 3])->get();
        $roles = Role::all();
        $schools = School::all();
        return view('users.form', compact('roles', 'panel_types', 'schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'national_code' => ['required', new NationalCodeFormat(), 'unique:users,national_code,NULL,id,deleted_at,NULL'],
            'phone' => 'required',
            'mobile' => ['required', new MobileFormat()],
            'role' => 'required|exists:roles,id',
            'panel_type' => 'required|exists:panel_types,id',
            'password' => 'required|confirmed|min:6',
            'school' => 'required',
            'levels' => [function ($attr, $val, $fail) {
                if (\request('panel_type') == 2 && $val == '') {
                    $fail('فیلد پایه الزامی است');
                }
            }],
        ]);
        try {

            $user = new User();
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->national_code = $request->national_code;
            $user->phone = $request->phone;
            $user->mobile = $request->mobile;
            $user->panel_type = $request->panel_type;
            $user->password = Hash::make($request->password);
            $user->school = (!empty($request->school)) ? $request->school : 0;
            $user->levels = (!empty($request->levels)) ? json_encode($request->levels) : '';
            $user->save();

            $user->assignRole($request->role);
            return response()->json([
                'status' => 100,
                'timer' => 500,
                'url' => route('users.index'),
                'msg' => trans('message.success'),
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $panel_types = PanelType::all();
        $roles = Role::all();
        $schools = School::all();
        $result = User::find($id);
        return view('users.form', compact('roles', 'panel_types', 'schools', 'result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'national_code' => ['required', new NationalCodeFormat(),'unique:users,national_code,'.$user->id.',id,deleted_at,NULL'],
            'phone' => 'required',
            'mobile' => ['required', new MobileFormat()],
            'role' => 'required|exists:roles,id',
            'panel_type' => 'required|exists:panel_types,id',
            'school' => [function ($attr, $val, $fail) {
                if (\request('panel_type') == 3 && $val == '') {
                    $fail('فیلد مدرسه الزامی است');
                }
            }],
        ]);
        try {

            $user = User::find($id);
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->national_code = $request->national_code;
            $user->phone = $request->phone;
            $user->mobile = $request->mobile;
            $user->panel_type = $request->panel_type;
            $user->levels = (!empty($request->levels)) ? json_encode($request->levels) : '';
            if ($request->has('password') && !empty($request->passwoard)) {
                $user->password = Hash::make($request->password);
            }
            $user->school = (!empty($request->school)) ? $request->school : 0;
            $user->save();
            DB::table('model_has_roles')->where('model_id', $id)->delete();
            $user->assignRole($request->role);
            return response()->json([
                'status' => 100,
                'timer' => 500,
                'url' => route('users.index'),
                'msg' => trans('message.success'),
            ]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = User::where('id', $request->id)->firstOrFail();
        if($user->panel_type==1){
            return response()->json(['status' => 500, 'msg' => 'این کاربر را نمی توانید حذف کنید']);
        };
        if (empty($user->expertSchoolId[0])) {
            $model = User::where('id', $user->id)->delete();
            if ($model)
                return response()->json(['status' => 100, 'msg' => 'کاربر با موفقیت حذف شد']);
            return response()->json(['status' => 500, 'msg' => 'خطا در انجام عملیات']);
        }
        return response()->json(['status' => 500, 'msg' => 'به دلیل اطلاعات وابسته امکان حذف وجود ندارد']);
    }
}
