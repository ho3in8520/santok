<?php

namespace App\Http\Controllers;

use App\Models\AcademicStudent;
use App\Models\School;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExpertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $levels = [];
        $user = auth()->user();
        if ($user->levels) {
            $levels = json_decode($user->levels);
        }

        $result = AcademicStudent::query()->whereHas('student');
        if ($request->has('first_name') && !empty($request->first_name)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('first_name', 'like', '%' . $request->first_name . '%');
            });
        }
        if ($request->has('last_name') && !empty($request->last_name)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('last_name', 'like', '%' . $request->last_name . '%');
            });
        }
        if ($request->has('father_name') && !empty($request->father_name)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('father_name', 'like', '%' . $request->father_name . '%');
            });
        }
        if ($request->has('date_birth') && !empty($request->date_birth)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('date_birth', $request->date_birth);
            });
        }
        if ($request->has('national_code') && !empty($request->national_code)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('national_code', $request->national_code);
            });
        }
        if ($request->has('school') && !empty($request->school)) {
            $result->where('school_id', $request->school);
        }

        $result = $result->whereIn('levels', $levels)->where('status', 0)->orderBy('id', 'desc')->paginate(10);
        $schools = School::all();
        return showData(view('expert.index', compact('result', 'schools')));
    }

    public function accept(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:academic_students,id'
        ]);
        $model = AcademicStudent::find($request->id);
        $model->accepted_at = Carbon::now();
        $model->accept_user_id = auth()->user()->id;
        $model->status = 1;
        $model->expert_school_id = $request->val;
        if ($model->save())
            return response()->json([
                'status' => 100,
                'timer' => 500,
                'msg' => trans('message.success'),
            ]);
        return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
    }

    public function reject(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:academic_students,id'
        ]);
        $model = AcademicStudent::find($request->id);
        $model->status = 2;
        $model->reject_reason = $request->reason;
        if ($model->save())
            return response()->json([
                'status' => 100,
                'timer' => 500,
                'msg' => trans('message.success'),
            ]);
        return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
    }

    public function majorInformation(Request $request)
    {
        $result = AcademicStudent::findOrFail($request->id);
        $majors = School::majorsSelect();
        $schools = School::all();
        return view('academic_years.major', compact('result', 'majors', 'schools'));
    }

    public function editStudent(Request $request)
    {
        $major_option = '';
        $result = AcademicStudent::findOrFail($request->id);
        $levels = $array = [
            1 => 'پیش دبستانی',
            2 => 'اول',
            3 => 'دوم',
            4 => 'سوم',
            5 => 'چهارم',
            6 => 'پنجم',
            7 => 'ششم',
            10 => 'نهم',
            11 => 'دهم',
        ];
        $majors = School::majorsSelect();
        $schools = School::all();
        if ($result->levels >= 10) {
            $school = School::findOrFail($result->expert_school_id);
            $majors_option = (!empty($school->major)) ? json_decode($school->major) : [];
            $major_option = '<option value="">انتخاب کنید...</option>';
            foreach ($majors_option as $item) {
                $major_option .= '<option value="' . $item . '" ' . ((!empty($result->expert_major) && $result->expert_major == $item) ? "selected" : "") . '>' . $item . '</option>';
            }
        }
        return view('academic_years.edit', compact('result', 'levels', 'schools', 'majors', 'major_option'));
    }

    public function majorSave(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:academic_students,id',
            'school' => 'required|exists:schools,id',
            'major' => 'required',
        ]);
        $model = AcademicStudent::find($request->id);
        $model->accepted_at = Carbon::now();
        $model->accept_user_id = auth()->user()->id;
        $model->status = 1;
        $model->expert_school_id = $request->school;
        $model->expert_major = $request->major;
        if ($model->save())
            return response()->json([
                'status' => 100,
                'timer' => 500,
                'msg' => trans('message.success'),
            ]);
        return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
    }

    public function updateStudent(Request $request)
    {
        $request->validate([
            'id' => 'required|exists:academic_students,id',
            'school' => 'required|exists:schools,id',
            'levels' => 'required|in:1,2,3,4,5,6,7,8,9,10,11',
            'major' => 'required_if:levels,>=,10',
        ]);
        $model = AcademicStudent::find($request->id);
        $model->accepted_at = Carbon::now();
        $model->accept_user_id = auth()->user()->id;
        $model->expert_school_id = $request->school;
        if ($request->levels >= 10) {
            $model->expert_major = $request->major;
        } else {
            $model->expert_major = null;
        }
        $model->levels = $request->levels;
        if ($model->save())
            return response()->json([
                'status' => 100,
                'timer' => 500,
                'msg' => trans('message.success'),
            ]);
        return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
    }

    public function majorSchool(Request $request)
    {
        $school = School::findOrFail($request->id);
        $majors = (!empty($school->major)) ? json_decode($school->major) : [];
        $html = '<option value="">انتخاب کنید...</option>';
        foreach ($majors as $item) {
            $html .= '<option value="' . $item . '">' . $item . '</option>';
        }
        return $html;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
