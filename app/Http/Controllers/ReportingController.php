<?php

namespace App\Http\Controllers;

use App\Exports\ExportExcel;
use App\Models\AcademicStudent;
use App\Models\City;
use App\Models\School;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function students(Request $request)
    {
        $states = City::where('parent', 0)->get();
        $schools = School::all();
        $result = [];

        $result = AcademicStudent::query()->whereHas('student');

        if ($request->has('first_name') && !empty($request->first_name)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('first_name', 'like', '%' . $request->first_name . '%');
            });
        }
        if ($request->has('last_name') && !empty($request->last_name)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('last_name', 'like', '%' . $request->last_name . '%');
            });
        }
        if ($request->has('father_name') && !empty($request->father_name)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('father_name', 'like', '%' . $request->father_name . '%');
            });
        }
        if ($request->has('date_birth') && !empty($request->date_birth)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('date_birth', $request->date_birth);
            });
        }
        if ($request->has('national_code') && !empty($request->national_code)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('national_code', $request->national_code);
            });
        }
        if ($request->has('phone') && !empty($request->phone)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('phone', $request->phone);
            });
        }
        if ($request->has('mobile') && !empty($request->mobile)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('mobile', $request->mobile);
            });
        }
        if ($request->has('state') && !empty($request->state)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('state', $request->state);
            });
        }
        if ($request->has('city') && !empty($request->city)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('city', $request->city);
            });
        }
        if ($request->has('levels') && !empty($request->levels)) {
            $result->where('levels', $request->levels);
        }
        if ($request->has('school') && !empty($request->school)) {
            $result->where('expert_school_id', $request->school);
        }
        if ($request->has('major') && !empty($request->major)) {
            $result->where('expert_major', $request->major);
        }
        if ($request->has('nationality') && !empty($request->nationality)) {
            $result->whereHas('student', function ($q) use ($request) {
                $q->where('nationality', $request->nationality);
            });
        }
        if ($request->has('export') && !empty($request->export)) {
            $result = $result->groupBy('student_id')->orderBy('id', 'desc')->get();
            if ($request->export == 'excel')
                return \exportExcel($this->dataToExport($result));
            return \exportPdf($this->dataToExport($result));
        }
        $result = $result->groupBy('student_id')->orderBy('id', 'desc')->paginate(10);
        return showData(view('reporting.students', compact('states', 'result', 'schools')));
    }


    protected function dataToExport($data)
    {
        $array = [];
        $nationality = ['foreign' => 'خارجی', 'iran' => 'ایرانی'];
        $i = 1;
        foreach ($data as $item) {
            array_push($array, [
                '#' => $i,
                'نام و نام خانوادگی' => $item->student->first_name . ' ' . $item->student->last_name,
                'نام پدر' => $item->student->father_name,
                'تاریخ تولد' => $item->student->date_birth,
                'کدملی' => $item->student->national_code,
                'پایه' => getLevels($item->levels),
                'محل تحصیل 1400-1399' => $item->school->name,
                'آدرس' => $item->student->fullAddress,
                'تابعیت' => $nationality[$item->student->nationality],
                'محل تحصیل 1401-1400' => !empty($item->expertSchool) ? $item->expertSchool->name : 'مشخص نشده',
                'رشته تحصیلی' => ($item->expert_major) ? $item->expert_major : 'مشخص نشده',
            ]);
            $i++;
        }
        return $array;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
