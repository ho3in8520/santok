<?php
/* string random */


use App\Exports\DataExport;
use App\Exports\ExportExcel;
use App\Models\School;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Admin\Entities\FactorMaster;
use Modules\Admin\Entities\UserGuard;
use Modules\User\Entities\Basket;
use Modules\User\Entities\Category;
use Modules\User\Entities\PanelType;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

include "custom_helper.php";

function stringRandom($number = 8)
{
    return substr(md5(uniqid(mt_rand(), true)), 0, $number);
}

//send sms
function sendSMS($numbers, $message)
{
    if (is_array($numbers)) {
        foreach ($numbers as $row) {
            $rule = ['iran_mobile'];
            if (!Validator::make(array($row), $rule)->errors()) {
                return false;
            }
        }
        Sms::sendSMS($numbers, $message);
        return true;
    } else {
        return false;
    }
}

/* flash session */
function flash($message, $level = 'success')
{
    session()->flash('flash_message', $message);
    session()->flash('flash_message_level', $level);
}

/* select array */
function selectArray($data, $valueOption, $option)
{
    $result = [];
    foreach ($data as $item) {
        $result[$item->$valueOption] = $item->$option;
    }
    return $result;
}

function groupingProduct($nameSearch = null)
{
    return \App\Http\Controllers\HelperController::groupingProduct($nameSearch);
}

function __old($inputName, $attribute)
{
    if (old($inputName) != "")
        return old($inputName);
    else if ($attribute != "")
        return $attribute;
    else
        return null;
}

function call_sp($sp_name, $param_count, $params = [])
{
    $sql = "SET NOCOUNT ON;exec {$sp_name} ";
    $i = 0;
    $sp_parameters = [];
    foreach ($params as $key => $value) {
        $sql .= "@{$key} = ?, ";
        array_push($sp_parameters, $value);
        $i++;
    }
    if ($i > 0)
        $sql = rtrim($sql, ', ');
    $result = \Illuminate\Support\Facades\DB::select($sql, $sp_parameters);

    return json_decode(json_encode($result), true);
}

function createRootData($rows)
{
    $values = "<Root>";
    foreach ($rows as $items) {
        $values .= "<Data ";
        foreach ($items as $key => $value) {
            $values .= "{$key}=\"{$value}\" ";
        }
        $values .= "/>";
    }
    $values = rtrim($values, " ") . "</Root>";
    return $values;
}

function index($data, $loop)
{
    return ($data->currentPage() - 1) * $data->perPage() + 1 + $loop->index;
}

function jdate_from_gregorian($input, $format = '%A, %d %B %Y | H:i:s')
{
    return \Morilog\Jalali\Jalalian::fromDateTime($input)->format($format);
}

function jalali_to_timestamp($date, $spliter = '/', $first = true)
{
    if ($first == true) {
        $h = "00";
        $i = "00";
        $s = "00";
    } else {
        $h = "23";
        $i = "59";
        $s = "59";
    }

    list($y, $m, $d) = explode($spliter, $date);
    return (new \Morilog\Jalali\Jalalian($y, $m, $d, $h, $i, $s))->toArray()['timestamp'];
}

function showData($view, array $array = [])
{
    if (request()->ajax()) {
        $temp = ["status" => "100", "FormatHtml" => $view->renderSections()['content']];
        $temp = array_merge($temp, $array);
        return json_encode($temp);
    } else
        return $view;
}

function parentCategory($data)
{

    $array = [];
    foreach ($data as $row) {

        $array [] = ['cat_id' => $row->id, 'cat_name' => $row->name, 'parent' =>
            (Category::find($row->id)->parent()->first()) ?
                parentCategory(Category::find($row->id)->topParent()->get()) :
                ""];
    }
    return array_reverse($array);
}


function breadcrumb($data)
{
    $array = '';

    foreach ($data as $row) {

        $array .= '<li><a href="#"><span>' . $row['cat_name'] . '</span></a></li>';

        if (is_array($row['parent']))
            breadcrumb($row['parent']);
    }
    echo $array;
}


function getItemBasket()
{
    if (Auth::guard(getCurrentGuard())->check()) {
        checkAndUpdateBasketUser();
        $basket = \Modules\Admin\Entities\FactorMaster::basket()->where("user_guard_id", getCurrentUser()->user_guard_id)->first();
//        checkAndUpdateBasketUser();
//        $basket = Basket::select([
//            'user_products.id as prod_id',
//            'user_products.slug_persian',
//            'user_products.name',
//            'user_products.price',
//            'basket_details.count',
//            \DB::raw("(select path from upload_routes where fk_id=user_products.id order By user_products.id asc limit 1) as pic"),
//        ])->where('basket.user_id', Auth::user()->id)
//            ->leftJoin('basket_details', 'basket_details.basket_id', 'basket.id')
//            ->leftJoin('user_products', 'user_products.id', 'basket_details.prod_id')
//            ->get()->toArray();

    } else {
        $basket = \Modules\Admin\Entities\FactorMaster::basket()->where("ip", request()->getClientIp())->first();
//        $basket = Basket::select([
//            'user_products.id as prod_id',
//            'user_products.slug_persian',
//            'user_products.name',
//            'user_products.price',
//            'basket_details.count',
//            \DB::raw("(select path from upload_routes where fk_id=user_products.id order By user_products.id asc limit 1) as pic"),
//        ])->where('basket.ip', request()->getClientIp())
//            ->leftJoin('basket_details', 'basket_details.basket_id', 'basket.id')
//            ->leftJoin('user_products', 'user_products.id', 'basket_details.prod_id')
//            ->get()->toArray();
    }
    return $basket;
}

function checkAndUpdateBasketUser()
{
    if (Auth::guard(getCurrentGuard())->check()) {

        \Modules\Admin\Entities\FactorMaster::basket()->where('ip', request()->getClientIp())->update(['user_guard_id' => getCurrentUser()->user_guard_id]);
    }
}

function firstItemBasket($variety)
{
    if (Auth::guard(getCurrentGuard())->check()) {

        $firstItem = \Modules\Admin\Entities\FactorMaster::basket()->with(['factorDetail' => function ($query) use ($variety) {
            $query->where('product_variety_id', $variety);
        }])->where('user_guard_id', getCurrentUser()->user_guard_id)->first();
//        $firstItem = Basket::select([
//            'basket.id',
//            'basket_details.count',
//            'user_products.slug_persian',
//        ])->where('basket.user_id', Auth::user()->id)
//            ->leftJoin('basket_details', 'basket_details.basket_id', 'basket.id')
//            ->leftJoin('user_products', 'user_products.id', 'basket_details.prod_id')
//            ->where('basket_details.prod_id', $prod_id)->first();
    } else {

        $firstItem = \Modules\Admin\Entities\FactorMaster::basket()->with(['factorDetail' => function ($query) use ($variety) {
            $query->where('product_variety_id', $variety);
        }])->where('ip', request()->getClientIp())->first();
//        $firstItem = Basket::select([
//            'basket.id',
//            'basket_details.count',
//            'user_products.slug_persian',
//        ])
//            ->where('basket.ip', request()->getClientIp())
//            ->leftJoin('basket_details', 'basket_details.basket_id', 'basket.id')
//            ->leftJoin('user_products', 'user_products.id', 'basket_details.prod_id')
//            ->where('basket_details.prod_id', $prod_id)->first();
    }
    return $firstItem;
}

function totalAmountBasket()
{
    $priceItem = [];
    $priceTotal = null;
    foreach (getItemBasket()->factorDetail as $row) {
        $price = $row->price * $row->count;
        array_push($priceItem, $price);
    }
    foreach ($priceItem as $row) {
        $priceTotal = $priceTotal + $row;
    }
    return $priceTotal;
}

function listCategory($data)
{
    $array = '';

    foreach ($data as $row) {
        $array .= '<li class="selected-category">' . $row['cat_name'] . '</li>';
        if (is_array($row['parent']))
            listCategory($row['parent']);
    }

    echo $array;
}

function customSelect($data)
{
    $customSelect = [];
    $customSelect[''] = __("validation.custom.select");
    foreach ($data as $key => $value) {
        $customSelect[$key] = $value;
    }
    return $customSelect;
}

function toast($message, $level = 'success')
{
    session()->flash('toast_message', $message);
    session()->flash('toast_message_level', $level);
}

function countFormNotComplete()
{
    if (!getCurrentUser()->panel_type)
        return 0;
    $sql = "SELECT COUNT(1) as cnt from fb_panel_type_forms LEFT JOIN fb_forms ON fb_forms.frm_id = fb_panel_type_forms.ptf_form_id WHERE fb_forms.enabled = 1 AND fb_panel_type_forms.pft_panel_type_id = :panel_type AND fb_panel_type_forms.deleted_at is null AND fb_panel_type_forms.ptf_form_id NOT IN (SELECT fb_answer_master.am_form_id from fb_answer_master where fb_answer_master.am_user_id=:user_id AND am_guard=:guard)";
    $result = DB::select($sql, ['panel_type' => getCurrentPanelType()->id, 'user_id' => getCurrentUser()->id, 'guard' => getCurrentGuard()]);
    return $result[0]->cnt;
}

function saveUserGuardId()
{
    if (empty(getCurrentUser()->user_guard_id)) {
        $guard = getCurrentGuard();
        $id = getCurrentUser()->id;
        $model = new UserGuard();
        $model->guard_name = $guard;
        $model->guard_user_id = $id;
        $model->save();
    }
}

function getCurrentUser()
{
    $guard = getCurrentGuard();
    if ($guard == "admin") {
        return auth('admin')->user();
    } elseif ($guard == "seller") {
        return auth('seller')->user();
    } elseif ($guard == "web") {
        return auth('web')->user();
    }
}

function getPrefix()
{
    $temp = ltrim(request()->route()->getPrefix(), "/");
    return str_replace('/', '.', $temp);
}

function adminPrefixes()
{
    $prefixes = [
        'manager',
        'bank',
    ];
    return $prefixes;
}

function getCurrentGuard()
{
    $prefix = getPrefix();
    if (in_array($prefix, ["manager", "bank"])) {
        return 'admin';
    } elseif ($prefix == "seller") {
        return 'seller';
    } else/*if (\Auth::guard('web')->check())*/ {
        return 'web';
    }
}

function findPanelTypeById($id)
{
    $panelType = PanelType::where("id", $id)->first();
    return $panelType;
}

function findPanelTypeByGuard($guard)
{
    $panelType = PanelType::where("guard", $guard)->first();
    return $panelType;
}

function getCurrentPanelType()
{
//    return \Modules\User\Entities\PanelType::where('guard', getCurrentGuard())->first();
    if (getCurrentGuard() == "web")
        return PanelType::where('id', getCurrentUser()->panel_type)->first();
    return null;
}

function pre($input)
{
    echo "<pre>";
    print_r($input);
    exit();
}

function getDistance($seller_id, $user_id)
{
    $result = DB::select(
        DB::raw("SELECT (
 111.111 *
    DEGREES(ACOS(LEAST(1.0, COS(RADIANS(sellers.location_lat))
         * COS(RADIANS(users.location_lat))
         * COS(RADIANS(sellers.location_lng - users.location_lng))
         + SIN(RADIANS(sellers.location_lat))
         * SIN(RADIANS(users.location_lat)))))) AS distance_in_km from sellers left join users on users.id= :user_id where sellers.id= :seller_id"), [
        'user_id' => $user_id,
        'seller_id' => $seller_id,
    ]);

    return $result[0]->distance_in_km;
}


function shippingCost()
{
    $sumPrice = 0;
    $arrayPrice = [];
    $factor = FactorMaster::basket()->where('user_guard_id', getCurrentUser()->user_guard_id)->first();
    if ($factor->send_mode == 1) {
        foreach ($factor->factorDetail as $row) {
            if ($row->shippingPrice != "-1" && $row->shippingPrice != "0") {
                $arrayPrice = collect($arrayPrice)->merge([$row->shippingPrice]);
            }
        }
        foreach ($arrayPrice as $row) {
            $sumPrice += $row;
        }
    }
    return $sumPrice;
}

function totalPriceFactor()
{
    return totalAmountBasket() + shippingCost();
}

function uploadImage($file, $type)
{
    $name = rand(11111, 99999) . '.' . $file->getClientOriginalExtension();

    $path = $file->storeAs('images/' . $type . '/' . getCurrentUser()->user_guard_id, $name, 'public');
    return $path;
}

function getImage($path, $mode = null)
{
    $ex = explode('/', $path);
    if ($mode) {
        $headers = array(
            'Content-Type: application/pdf',
        );
        return Response::download($path, 'rezome.pdf', $headers);
    }
    return route("image.view", ['type' => $ex[1], 'id' => $ex[2], 'file' => $ex[3]]);
}

function getSettingBank()
{
    $result = \Modules\Admin\Entities\Bank\BankSetting::where('id', 1)->first();
    return $result;
}

function nextStep($step)
{
    $panel_type = getCurrentPanelType()->table_name;
    $path = "../Modules/User/Resources/views/steps/" . $panel_type;
    $files = array_diff(scandir($path), ['.', '..']);
    $temp = [];
    foreach ($files as $file) {
        if (strstr($file, '.blade.php')) {
            $file = rtrim($file, ".blade.php");
            if ((int)$file > $step)
                array_push($temp, $file);
        }
    }
    $address = '/' . $panel_type . "/" . "dashboard";
    if (!empty($temp)) {
        $address = '/' . $panel_type . '/steps/' . $temp[0];
    } else {
        $user = getCurrentUser();
        $user->is_complete_steps = 1;
        $user->save();
    }

    return $address;

}

function getViewNewsUser()
{
    if (Auth::guard(getCurrentGuard())->check()) {
        checkAndUpdateViewNews();
        return \Modules\Admin\Entities\PostViews::where('user_guard_id', getCurrentUser()->user_guard_id)->orWhere('ip', request()->getClientIp())->first();
    } else {
        return \Modules\Admin\Entities\PostViews::where('ip', request()->getClientIp())->first();
    }
}

function checkAndUpdateViewNews()
{
    $views = \Modules\Admin\Entities\PostViews::where('ip', request()->getClientIp())->where('user_guard_id', null)->get();
    if ($views) {
        foreach ($views as $view) {
            \Modules\Admin\Entities\PostViews::where('post_id', $view->post_id)->update(['user_guard_id' => getCurrentUser()->user_guard_id]);
        }
    }
}

function renameFileManager($event)
{
    $temp = config("file-manager.basePath") . decrypt($event->disk()) . DIRECTORY_SEPARATOR . $event->path() . DIRECTORY_SEPARATOR;
    $path = $temp . $event->files()[0]['name'];
    $new_path = $temp . time() . '.' . $event->files()[0]['extension'];
    return array($path, $new_path);
}

function get_file_extension($file_name)
{
    return substr(strrchr($file_name, '.'), 1);
}

function exportPdf($data)
{
    $data = [
        'data' => $data
    ];
    $pdf = PDF::loadView('templates.pdf.index', $data);
    return $pdf->stream('reporting.pdf');
}

function exportExcel(array $array, $fileName = 'reporting.xlsx')
{
    return Excel::download(new ExportExcel($array), $fileName);
}

function convertEnglishToPersian($string)
{
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

    $output = str_replace($english, $persian, $string);
    return $output;
}

function generateLinkDownload($referrer_id, $upload_id)
{
    $find = \Modules\Admin\Entities\DownloadLink::where('user_guard_id', getCurrentUser()->user_guard_id)->where('referrer_id', $referrer_id)->first();
    if ($find && $find->time_expire > \Carbon\Carbon::now()) {
        return $find->token;
    }
    $time = \Carbon\Carbon::now()->addHours(3);
    $token = uniqid(\Illuminate\Support\Str::random(128), true);
    $model = new \Modules\Admin\Entities\DownloadLink();
    $model->referrer_id = $referrer_id;
    $model->upload_route_id = $upload_id;
    $model->token = $token;
    $model->user_guard_id = getCurrentUser()->user_guard_id;
    $model->time_expire = $time;
    if ($model->save())
        return $token;
    return false;
}

function deleteStream($id)
{
    $result = \Illuminate\Support\Facades\Http::withoutVerifying()->withHeaders(['Authorization' => env('API_KEY_ARVAN'), 'Content-Type' => 'application/json'])->delete('https://napi.arvancloud.com/live/2.0/streams/' . $id);
    return $result->status();
}

function generateRandomString($length = 10)
{
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getSummary($data)
{
    if (count($data) > 0) {
        return "نمایش " . "<b>" . ($first = (($data->currentPage() - 1) * $data->perPage() + 1)) . "</b>  تا " . "<b>" . ($first + count($data) - 1) . "</b>" . " مورد از کل " . "<b>" . $data->total() . "</b>" . " مورد.";
    } else
        return "نمایش <b>0</b> مورد.";
}

function getLevels($levels)
{
    $array = [
        1 => 'پیش دبستانی',
        2 => 'اول',
        3 => 'دوم',
        4 => 'سوم',
        5 => 'چهارم',
        6 => 'پنجم',
        7 => 'ششم',
        8 => 'هفتم',
        10 => 'نهم',
        11 => 'دهم',
    ];
    return $array[$levels];
}

function academicOrientation()
{
    $array = [
        'کاردانش -صنعت',
        'کاردانش -خدمات',
        'کاردانش -کشاورزی',
        'کاردانش -هنر',
        'فنی و حرفه ای - صنعت',
        'فنی و حرفه ای - خدمات',
        'فنی و حرفه ای - کشاورزی',
        'فنی و حرفه ای - هنر',
        'نظری - ریاضی و فیزیک',
        'نظری - علوم تجربی',
        'نظری - ادبیات و علوم انسانی',
        'نظری - علوم و معارف اسلامی',
        'هیچ کدام',
    ];
    $html = '<option value="">انتخاب کنید...</option>';
    foreach ($array as $item) {
        $html .= '<option value="' . $item . '">' . $item . '</option>';
    }
    return $html;
}

function getAllMajors($option=true)
{
    $majors = School::majorsSelect($option);
    return $majors;
}
