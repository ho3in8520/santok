<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AcademicStudent extends Model
{
    use HasFactory;

    public function student()
    {
        return $this->belongsTo(Student::class,'student_id');
    }

    public function school()
    {
        return $this->belongsTo(School::class,'school_id');
    }

    public function expertSchool()
    {
        return $this->belongsTo(School::class,'expert_school_id');
    }

    public function academicYear()
    {
        return $this->belongsTo(AcademicYear::class,'academic_year_id');
    }
}
