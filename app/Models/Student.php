<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory;
    protected $fillable = [
        'first_name',
        'last_name',
        'father_name',
        'date_birth',
        'national_code',
        'mobile',
        'city',
        'nationality',
        'zone',
        'street',
        'school_id',
        'state',
    ];
    public function files()
    {
        return $this->morphMany('App\Models\UploadedFile', 'uploadable');
    }

    public function stateModel()
    {
        return $this->belongsTo(City::class, 'state');
    }

    public function cityModel()
    {
        return $this->belongsTo(City::class, 'city');
    }

    public function academicStudent()
    {
        return $this->hasMany(AcademicStudent::class, 'student_id');
    }

    public function getFullAddressAttribute()
    {
        return (!empty($this->stateModel->name)?'استان ' . $this->stateModel->name :'') . (!empty($this->cityModel->name)?' شهر ' . $this->cityModel->name:'') . ' منطقه ' . $this->zone /*. ' خیابان ' . $this->street . ((!empty($this->alley)) ? ' کوچه ' . $this->alley : '') . ((!empty($this->deadend)) ? ' بن بست ' . $this->deadend : '') . ' پلاک ' . $this->plaque*/;
    }
}
