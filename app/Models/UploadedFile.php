<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UploadedFile extends Model
{
    use HasFactory;

    protected $fillable = ["user_id", "uploadable_id", "uploadable_type", "path", "thumbnail_path", "status", "state", "description", "mime_type", "type"];

    public function uploadable()
    {
        return $this->morphTo(__FUNCTION__, "uploadable_type", "uploadable_id");
    }
}
