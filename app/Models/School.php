<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;

    public function stateModel()
    {
        return $this->belongsTo(City::class, 'state');
    }

    public function cityModel()
    {
        return $this->belongsTo(City::class, 'city');
    }

    public static function majorsSelect($option = true)
    {
        $data = [];
        $html = '';
        $result = School::select('major')->where('status', 1)->whereNotNull('major')->get();
        foreach ($result as $row) {
            foreach (json_decode($row->major) as $item) {
                $data = array_merge($data, [$item]);
            }
        }
        $data = array_unique($data);
        if (!$option)
            return $data;
        $html = '<option value="">انتخاب کنید...</option>';
        foreach ($data as $row) {
            $html .= '<option value="' . $row . '">' . $row . '</option>';
        }
        return $html;
    }
}
