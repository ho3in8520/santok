<?php

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
return redirect()->route('login.form');
});

Route::get('get-cities', function () {
    $cities = \App\Models\City::select(['id', 'name'])->where('parent', request('id'))->get();
    $html = '<option value="">انتخاب کنید...</option>';
    foreach ($cities as $row) {
        $html .= '<option value="' . $row->id . '">' . $row->name . '</option>';
    }
    return json_encode($html);
})->name('get-cities');

Route::get('avatar/{id}/{filename}', function ($id, $filename) {
    $path = storage_path('app/avatars/' . $id . '/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
})->name('get-avatar');
Route::namespace('App\Http\Controllers\Auth')->middleware('guest')->group(function () {
    Route::get('login', "LoginController@loginForm")->name('login.form');
});

Route::namespace('App\Http\Controllers')->middleware('auth')->group(function () {
    Route::get('logout', "Auth\LoginController@logout")->name('logout');
    Route::get('dashboard', "HomeController@dashboard")->name('dashboard');
    Route::resource('school', "SchoolController")->names('school');
    Route::resource('student', "StudentController")->names('student');
    Route::get('academic/years', "AcademicYearsController@show")->name('academic.years');
    Route::get('academic/years/{id}/edit', "AcademicYearsController@edit")->name('academic.years.edit');
    Route::get('roles', "RolesController@index")->name('roles.index');
    Route::get('roles/create', "RolesController@create")->name('roles.create');
    Route::post('roles/store', "RolesController@store")->name('roles.store');
    Route::get('roles/{id}/edit', "RolesController@edit")->name('roles.edit');
    Route::post('roles/{id}/update', "RolesController@update")->name('roles.update');
    Route::delete('roles/delete', "RolesController@destroy")->name('roles.destroy');
    Route::get('users', "UserController@index")->name('users.index');
    Route::get('users/create', "UserController@create")->name('users.create');
    Route::post('users/store', "UserController@store")->name('users.store');
    Route::get('users/{id}/edit', "UserController@edit")->name('users.edit');
    Route::post('users/{id}/update', "UserController@update")->name('users.update');
    Route::delete('users/delete', "UserController@destroy")->name('users.destroy');
    Route::get('student-school/search', "StudentSchoolController@search")->name('student.school.search');
    Route::post('student-school/search/response', "StudentSchoolController@searchResponse")->name('student.school.search.response');
    Route::get('student-school/create/{id}', "StudentSchoolController@create")->name('student.school.create');
    Route::post('student-school/store', "StudentSchoolController@store")->name('student.school.store');
    Route::get('student-school', "StudentSchoolController@index")->name('student.school.index');
    Route::get('expert/student-school', "ExpertController@index")->name('expert.student.school.index');
    Route::post('expert/student-school/accept', "ExpertController@accept")->name('expert.student.school.accept');
    Route::post('expert/student-school/reject', "ExpertController@reject")->name('expert.student.school.reject');
    Route::get('reporting/students', "ReportingController@students")->name('reporting.students');
    Route::get('student-school/inquiry', "StudentSchoolController@inquiry")->name('student.school.inquiry');
    Route::get('student-school/inquiry-search', "StudentSchoolController@inquiryResponse")->name('student.school.inquiry.response');
    Route::get('student-school/major-information', "ExpertController@majorInformation")->name('student.school.major.information');
    Route::get('student-school/major-school', "ExpertController@majorSchool")->name('student.school.major.school');
    Route::post('student-school/major/save', "ExpertController@majorSave")->name('student.school.major.save');
    Route::get('student-school/edit', "ExpertController@editStudent")->name('student.school.edit.student');
    Route::post('student-school/update', "ExpertController@updateStudent")->name('student.school.update.student');
});

