// window.livewire.on('alert', param => {
//     toastr[param['type']](param['message']);
//     if (param['url']) {
//         setTimeout(function () {
//             location.href = param['url'];
//         }, param['time'])
//     }
// });

window.livewire.on('alert', param => {
    switch (param['type']) {
        case 'success':
            alertify.success(param['message']);
            break
        case 'error':
            alertify.error(param['message']);
            break
        case 'warning':
            alertify.warning(param['message']);
            break
        case 'info':
            alertify.info(param['message']);
            break
        case 'message':
            alertify.message(param['message']);
    }
    if (param['url']) {
        setTimeout(function () {
            location.href = param['url'];
        }, param['time'])
    }
});

window.livewire.on('swal', param => {
    if (param['question']) {
        swal({
            title: param['title'],
            text: param['message'],
            icon: param['type'],
            buttons: ['انصراف','تایید'],
        })
            .then((willDelete) => {
                if (willDelete) {
                    window.livewire.emit(param['question'],param['params']);
                }
            });
    } else {
        swal({
            title: param['title'],
            text: param['message'],
            icon: param['type'],
        });
    }
    if (param['url']) {
        setTimeout(function () {
            location.href = param['url'];
        }, param['time'])
    }
});

window.livewire.on('goToPage', () => {
    lazyImagesInit();
});

$(document).on('change',".datePicker",function (){
    window.livewire.emit('datePicker',$(document).find(".datePicker").val())
})



