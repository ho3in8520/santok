classNameErrorSelect2 = "select2-selection--single";
classNameErrorSelect2Multiple = "select2-selection--multiple";
TimeOutActionNextStore = 2000;
buttonLoadingText = 'لطفا منتظر بمانید....';
buttonTextSweetAlert = "باشه";
textConfirmDelete = "آیا از حذف این مورد اطمینان دارید ؟";
buttonTextConfirm = "بلی";
elemClicked = null;
//for reset modal content and sections
objectId = "";
url = "";
messageMode = "swal";


$(':input').on('focus', function () {
    $(this).attr('autocomplete', 'off');
});

function resetFormInput(elementClick) {
    $(elementClick).closest('form').find('input[type=text],input[type=number],input[type=password],textarea,.hiddenEmpty').val("");
    $(elementClick).closest('form').find('select').val("").trigger("change");
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function startConfig(showProgress = false) {
    $('.help-block').remove();
    $("." + classNameErrorSelect2).attr("style", normalCss);
    $("#progressBar").css('width', 0 + '%');
    $("#progressBar").css('background-color', '#3399ff');
    $("input[type=text],select,textarea,input[type=password]").attr("style", normalCss);
    (showProgress) ? $("#progressShow").show() : false;

}

function ajaxStore(e) {
    e.preventDefault();
    var elementClick = $(this);
    var reloadPage = $(this).data('reload');
    var callback = $(this).data('callback');
    if ($(elementClick).attr("data-massage-mode") != undefined)
        messageMode = $(elementClick).attr("data-massage-mode");
    startConfig();
    var captionButton = $(elementClick).html();
    $(elementClick).html(' <i class="fa fa-circle-o-notch fa-spin"></i>' + buttonLoadingText);
    if (typeof CKEDITOR != 'undefined') {
        for (instance in CKEDITOR.instances)
            CKEDITOR.instances[instance].updateElement();
    }
    var data = new FormData($(elementClick).closest('form')[0]);
    // var data=$(elementClick).closest('form').serialize()
    data.append('status', $(elementClick).val());
    $.ajax({
        url: $(elementClick).closest('form').attr('action'),
        type: 'POST',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            $(elementClick).html(captionButton)
            resultResponse(response, reloadPage);
            if (elementClick.hasClass('reset-form')) {
                if (objectId != "")
                    getHttpRequestForModal(url, {book_id: objectId}, {size: 'modal-xl'});

            }
            $(function () {
                afterSave();
            });
        },
        error: function (xhr) {
            $(".ajaxMessage").html("");
            $(elementClick).html(captionButton)
            defaultProgress();
            errorForms(xhr);
        },
        complete: function (data) {

        }
    });
}

///////////////////////////////////////////////////////////////////////////////result response
function resultResponse(result, reloadPage = true) {
    switch (parseInt(result.status)) {
        case 100:
            $("#progressBar").css('background-color', "green");
            if ("msg" in result && result.msg != "")
                alertMessage('success', result.msg, messageMode);
            if ("url" in result) {
                setTimeout(function () {
                    window.location.href = result.url;
                }, ("timer" in result) ? result.timer : TimeOutActionNextStore);
            } else if ("data" in result) {
                $(".showHtml").html(result.data)
            } else if (reloadPage) {
                setTimeout(function () {
                    window.location.reload();
                }, ("timer" in result) ? result.timer : TimeOutActionNextStore);
            }
            break;
        case 200:
            $(".message-end-warranty").html(result.msg);
            $(".show-message").slideDown(1000);
            break;
        case 300:
            alertMessage('alert', result.msg, messageMode)
            if ("url" in result) {
                setTimeout(function () {
                    window.location.href = result.url;
                }, TimeOutActionNextStore);
            }
            break;
        case 422:
            alertMessage('error', result.msg, messageMode);
            defaultProgress();
            break;
        case 403:
            alertMessage('warning', result.msg, messageMode);
            break;
        case 500:
            alertMessage('error', result.msg, messageMode);
            defaultProgress();
            break;
    }

}

///////////////////////////////////////////////////////////////////////////////show message
function alertMessage(typeMessage, message, mode = "swal") {
    if (mode == "swal") {
        var messageShow = "";
        switch (typeMessage) {
            case 'success' :
                swal({
                    title: "",
                    text: message,
                    icon: "success",
                    button: buttonTextSweetAlert,
                });
                break;
            case 'error' :
                swal({
                    title: "",
                    text: message,
                    icon: "error",
                    button: buttonTextSweetAlert,
                });
                break;
            case 'warning':
                swal({
                    title: "",
                    text: message,
                    icon: "warinig",
                    button: buttonTextSweetAlert,
                });
                break;
            case 'alert':
                alert('لطفا منتظر بمانید...')
                break;
        }
        $(".ajaxMessage").html(messageShow);
    } else {
        switch (typeMessage) {
            case 'success' :
                toastr.success(message, "موفق", {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: TimeOutActionNextStore
                });
                break;
            case "error":
                toastr.error(message, "خطا", {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: TimeOutActionNextStore
                });
                break;
            case "warning":
                toastr.warning(message, "هشدار", {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: TimeOutActionNextStore
                });
                break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////default progrees
function defaultProgress() {
    $("#progressBar").css('width', '0');
}

/////////////////////////////////////////////////////////////////////////////////error validation form
function errorForms(xhr) {
    if (xhr.status === 422) {
        alertMessage('error', 'لطفا خطاهای فرم را برطرف نمایید');
        var errorsValidation = JSON.parse(xhr.responseText).errors;
        $.each(errorsValidation, function (key, value) {
            var errArray = key.split('.');
            console.log(errArray[1]);
            var getElement = "";
            var errorHtml = "<div class='help-block text-danger'>" + value + "</div>";
            if (typeof errArray[1] === 'undefined') {
                getElement = "[name='" + key + "']";
                showHtmlValidation(getElement, errorHtml)
                getElement = "[name='" + key + "[]" + "']";
                showHtmlValidation(getElement, errorHtml)
            } else if (!$.isNumeric(errArray[1])) {
                getElement = "[name='" + (errArray[0] + "[" + errArray[1] + "]']");
                showHtmlValidation(getElement, errorHtml)
            } else if ($.isNumeric(errArray[1])) {
                getElement = "[name^='" + errArray[0] + "']";
                showHtmlValidation(getElement, errorHtml, errArray[1])
            }
        });
    }
}

////////////////////////////////////////////////////////////////////////////////show html error validation
function showHtmlValidation(getElement, errorHtml, indexArr = "") {
    var element = $(getElement);
    if (indexArr != "") {
        element = element.eq(indexArr);
    }
    if ($(element).closest(".input-group").length)
        element = element.closest(".input-group");

    if (!$(getElement).parent().hasClass('select2'))
        if ($(getElement).parent().find('.' + classNameErrorSelect2Multiple).length)
            element = element.parent().find('.' + classNameErrorSelect2Multiple);
        else if ($(getElement).parent().find('.' + classNameErrorSelect2).length)
            element = element.parent().find('.' + classNameErrorSelect2);
    element.attr("style", errorCss);
    element.after(errorHtml);

    /*if (!$(getElement).hasClass('select2')) {
        if (indexArr != "") {
            //indexArr++;
            if ($(getElement).eq(indexArr).closest(".input-group").length) {
                $(getElement).eq(indexArr).closest(".input-group").after(errorHtml);
                $(getElement).eq(indexArr).closest(".input-group").attr("style", errorCss)
            } else {
                $(getElement).eq(indexArr).after(errorHtml);
                $(getElement).eq(indexArr).attr("style", errorCss)
            }
        } else {
            if ($(getElement).closest(".input-group").length) {
                $(getElement).closest(".input-group").after(errorHtml);
                $(getElement).closest(".input-group").attr("style", errorCss)
            } else {
                $(getElement).after(errorHtml);
                $(getElement).attr("style", errorCss)
            }
        }
    } else {
        if ($(getElement).eq(indexArr).closest(".input-group").length) {
            $(getElement).eq(indexArr).closest(".input-group").after(errorHtml);
            $(getElement).eq(indexArr).closest(".input-group").attr("style", errorCss)
        } else {
            $(getElement).eq(indexArr).parent().find('.' + classNameErrorSelect2Multiple).attr("style", errorCss)
            $(getElement).eq(indexArr).parent().find('.' + classNameErrorSelect2Multiple).after(errorHtml);
            $(getElement).eq(indexArr).parent().find('.' + classNameErrorSelect2).attr("style", errorCss)
            $(getElement).eq(indexArr).parent().find('.' + classNameErrorSelect2).after(errorHtml);
        }
    }*/
}

$(document).on('click', '.timeOutChangePage', function (e) {
    e.preventDefault();
    var hrefLink = $(this).attr('href');
    alertMessage('alert', 'لطفا کمی منتظر بمانید...');
    setTimeout(function () {
        window.location.href = hrefLink;
    }, TimeOutActionNextStore);

});

////////////////////////////////////////////////////////////////////////delete ajax

function ajaxDelete(url, elementDelete) {
    swal({
        title: textConfirmDelete,
        text: "",
        icon: "warning",
        buttons: buttonTextConfirm,
    })
        .then((willDelete) => {
            if (willDelete) {
                var id = elementDelete.closest('tr').attr('data-id');
                var path = url.replace('?', id);
                $.ajax({
                    'url': path,
                    type: 'DELETE',
                    success(response) {
                        swal(response.msg, {
                            icon: "success",
                        });
                        elementDelete.closest('tr').slideUp(1000)
                    }
                });

            }
        });
};

///////////////////////////////////////////////////////////////////////ajax store use
$(document).on('click', '.ajaxStore', ajaxStore);

////////////////////////////////////////////////////////////////////////http request and get response and put in modal
function getHttpRequestForModal(url, parameter = {}, modalConfig = {}) {
    modalSize = modalConfig.hasOwnProperty('size') ? modalConfig.size : 'modal-lg';
    modalHeader = modalConfig.hasOwnProperty('header') ? modalConfig.header : '';
    createModal({header: modalHeader, size: modalSize});
    $.get(url, parameter, function (response) {
        if (parseInt(response.status) == 100) {
            $(".modal-body").html(response.htmlForModal);
            if ($("#myModal .modal-body").find('.ck-editor').length) {
                CKEDITOR.replace($('.ck-editor').attr('name'), {
                    filebrowserUploadUrl: '/upload_ck'
                });
            }
            $('#myModal').modal({backdrop: 'static', keyboard: false});

            setTimeout(function () {
                $('#myModal').find('.select2').select2()
            }, 1000)
        }

    });


}

/////////////////////////////////////////////////////////////////////////////////modal creator
function createModal(option = {}) {
    modalSize = option.hasOwnProperty('size') ? option.size : 'modal-sm';
    modalHeader = option.hasOwnProperty('header') ? option.header : '';

    if ($("#myModal").length == 0) {
        var modalHtml = '  <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">\n' +
            '        <div class="modal-dialog ' + option.size + '">\n' +
            '            <div class="modal-content">\n' +
            '                <div class="modal-header">\n' +
            '                    <h4 class="modal-title">' + option.header + '</h4>\n' +
            '                </div>\n' +
            '                <div class="modal-body">\n' +
            '                </div>\n' +
            '               <div class="modal-footer">\n' +
            '                  <button type="button" class="btn btn-danger" data-dismiss="modal">انصراف</button>\n' +
            '               </div>' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>';
        $("#createModal").html(modalHtml);
        $("#myModal").modal('toggle');
    } else {
        $("#myModal").find('.modal-dialog').addClass(modalSize);
        $("#myModal").find('.modal-header').html(modalHeader);
        if (!$("#myModal").hasClass("show")) {
            $("#myModal").modal('show');
        }

    }

}

/////////////////////////////////////////////////////////////////////////////////css validation form and create div show error
var errorCss = "border-width: 1px; border-color: #d2322d; border-style: solid";
var normalCss = "border-width: 1px; border-color: #75787D; border-style: solid";

//////////////////////////////////////////////////////////////////////////////// empty content modal next hide modal

$(document).on('hidden.bs.modal', '#myModal', function () {
    $('#myModal .modal-body').html("")
});


$(document).on('keyup', '.number-format', function () {
    var elementKeyUp = $(this);
    elementKeyUp.val(numberFormat(elementKeyUp.val()))

});

function numberFormat(Number) {
    Number += '';
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    x = Number.split('.');
    y = x[0];
    z = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(y))
        y = y.replace(rgx, '$1' + ',' + '$2');
    return y + z;
}

$(document).on("click", "a[data-confirm]", function (e) {
    e.preventDefault();
    var elem = $(this);
    if (confirm($(elem).data('confirm')) == false)
        return false;
    if ($(elem).data('method')) {
        var options = {
            'action': $(elem).data('action'),
            'target': '_self'
        };
        if ($(elem).data('method').toLowerCase() != "delete")
            $.extend(options, {
                method: $(elem).data('method')
            });
        else
            $.extend(options, {
                method: "post"
            });

        var form = $('<form>', options);
        if ($(elem).data('method').toLowerCase() == "delete") {
            form.append($('<input>', {
                'name': '_method',
                'value': "delete",
                'type': 'hidden'
            }));
        }
        form.append($('<input>', {
            'name': '_token',
            'value': $('meta[name=csrf-token]').attr('content'),
            'type': 'hidden'
        }));
        if ($(elem).data('id')) {
            form.append($('<input>', {
                'name': 'id',
                'value': $(elem).data('id'),
                'type': 'hidden'
            }));
        }
        form.appendTo(document.body);
        $(form).submit();
    }
});

$(document).on("change", "select.state", function () {
    var id = $(this).val();
    $('select.city').html($("<option value=''>انتخاب کنید...</option>"));
    $.get("/get-city", {id: id}, function (data) {
        $.each(data, function (key, value) {
            $('select.city')
                .append($("<option></option>")
                    .attr("value", value.id)
                    .text(value.name));
        });
    })
});

$(document).on("click", ".btn-grid-export", function (e) {
    e.preventDefault();
    var btn = $(this);
    var type = $(this).closest(".export-holder").find("select").val();
    var url = updateQueryStringParameter(window.location.href, "export-mode", type);
    var win = window.open(url, '_blank');
    if (win) {
        win.focus();
    } else {
        alert('Please allow popups for this website');
    }
});


function updateQueryStringParameter(url, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = url.indexOf('?') !== -1 ? "&" : "?";
    if (url.match(re)) {
        return url.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return url + separator + key + "=" + value;
    }
}

$(':input:enabled:visible:first').focus();

// $(document).find("input[type=hidden]").each(function (i) { $(this).attr('tabindex', 0); });
/*$(":input:not(:hidden)").each(function (i) { $(this).attr('tabindex', i + 1); });*/

/*$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });*/
    $(".verify-code").keyup(function () {
        var count = $(this).val().length;
        if (count == 5) {
            $('.ajaxStore').click();
        }
    })
// });
