$(document).ready(function () {
    $('.select2').select2();
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

$(document).on('focus', ".datePicker", function () {
    $(this).datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        isRTL: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy/mm/dd",
        showAnim: 'slideDown',
        showButtonPanel: true,
        yearRange: "-100:+10",
    });
});
$(document).on('click', '.export', function () {
    var export_type = $(this).data('export')
    var url = $(document).find('form').attr('action');
    var data = $(document).find('form').serializeArray();
    var query = $.param(data);
    console.log(url + "?" + query)
    window.open(url + "?" + query + '&export=' + export_type, '_blank');
});
